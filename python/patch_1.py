from itertools import product

import numpy as np

def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays.
    """

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out

def unroll_idxs(idxs):
    '''unrolls indexs for flat array access'''
    return [zip(*cartesian(idx)) for idx in idxs]


def make_range(xs, patch_dim):
    return np.array([range(x, x + patch_dim) for x in xs])


def random_idxs(dims, patch_dims, N):
    idxs = []
    for dim, patch_dim in zip(dims, patch_dims):
        start_idxs = np.random.randint(0, dim - patch_dim + 1, N)
        ranges = make_range(start_idxs, patch_dim)
        idxs.append(ranges)

    return np.array(zip(*idxs))


def slide_idxs(dims, patch_dims, slide_dims):
    idxs = []
    for dim, patch_dim, slide_dim in zip(dims, patch_dims, slide_dims):
        start_idxs = range(0, dim - patch_dim + 1, slide_dim)
        ranges = make_range(start_idxs, patch_dim)
        idxs.append(ranges)

    return np.array(list(product(*idxs)))


def ix(xs, idxs, dims):
    '''n-dimensional array slicing for list of lists
       xs is n-dimensional input
       idxs a  list of blocks (e.g. from random_idxs)
       dims is the output dimensions
    '''
    idxs = unroll_idxs(idxs)
    patches = np.array([xs[idx] for idx in idxs])

    return patches.reshape(-1, *dims)


def putback(p, dims, patch_dims, sampling_method='slide',
          N=100, slide_dims=None):
    if slide_dims is None:
        slide_dims = [1]*len(dims)
    # print p.shape
    x = np.zeros(dims)
    count = np.zeros(dims)
    idxs = slide_idxs(dims, patch_dims, slide_dims)
    # print "dims.shape",len(dims)
    # print "idxs.shape",idxs.shape
    sl = [slice(0, patch_dims[j]) for j in range(len(patch_dims))]
    for i in range(idxs.shape[0]):
        xidx = tuple([slice(idxs[i,j][0], idxs[i,j][-1]+1)
                      for j in range(len(dims))])
        # print xidx
        count[xidx] += 1
        # print tuple([i]+xidx),[i]+xidx
        # print p[tuple([i]+xidx)].shape
        # print x[tuple(xidx)].shape
        x[xidx]+=p[tuple([i]+sl)]
    x/=count
    return x

def patch(xs, patch_dims, sampling_method='random',
          N=100, slide_dims=None):
    '''n-dimensional patching function
       xs is an n-dimensional array of data,
       the first dimension denotes the number
       of samples, the rest of the dimensions
       denotes the shape of each sample.

       patch_dims is the dimensions of the feature
       to extract.

       sampling_method is the method used to extract
       the patches.
       'random' randomly samples N patches for each input
       'slide' takes sliding patches in each dimension and
       requires slide_patch_dims to denote the stride of each slide
       'block' is a special case of sliding with the sliding_patch_dims
       equal to the patch_dims

        N is the number of random samples per sample for 'random'

        slide_dims is the sliding dimensions for 'slide'

    '''

    if not sampling_method in ['random', 'slide', 'block']:
        print 'Invalid sampling method'
        assert False


    if sampling_method is 'random':
        patches = []
        for x in xs:
            dims = x.shape
            idxs = random_idxs(dims, patch_dims, N)
            patches.append(ix(x, idxs, patch_dims))

        return np.array(patches)


    elif sampling_method is 'slide':

        if slide_dims is None:
            slide_dims = [1]*len(xs[0].shape)

        patches = []
        for x in xs:
            dims = x.shape
            idxs = slide_idxs(dims, patch_dims, slide_dims)
            patches.append(ix(x, idxs, patch_dims))

        return np.array(patches)


    elif sampling_method is 'block':
        slide_dims = patch_dims

        patches = []
        for x in xs:
            dims = x.shape
            idxs = slide_idxs(dims, patch_dims, slide_dims)
            patches.append(ix(x, idxs, patch_dims))

        return np.array(patches)



if __name__=='__main__':
    samples = np.arange(4000).reshape(4, 10, 10, 10)
    patch_dims = [2, 2, 2]

    print '{} samples with shape: {}'.format(samples.shape[0],
                                             samples.shape[1:])
    print 'terreact patching!'

    print 'Using patch dims: ', patch_dims
    print 'With random sampling with 10 per sample'
    patches = patch(samples, patch_dims, sampling_method='random', N=10)
    print patches.shape

    print 'With slide sampling moving 1 in each direction'
    patches = patch(samples, patch_dims, sampling_method='slide')
    print patches.shape

    print 'With block sampling'
    patches = patch(samples, patch_dims, sampling_method='block')
    print patches.shape
