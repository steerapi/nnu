import math

def mse(imga_int, imgb_int, x, y):
    """Return the mean square error between the two images."""

    tmp = sum((a - b) ** 2 for a, b in zip(imga_int, imgb_int))
    _mse = float(tmp) / x / y

    return _mse

def psnr(imga_int, imgb_int, x, y):
    """Calculate the peak signal-to-noise ratio."""

    _psnr = 20 * math.log(255 / math.sqrt(mse(imga_int, imgb_int, x, y)), 10)
    return _psnr

def im2int(imga_float):
    return [min(255, int(i * 255)) for i in imga_float];

def im2float(imga_int):
    return [min(1.0, float(i / 255.0)) for i in imga_int];

# print im2int([0.0392156862745098, 0.0784313725490196, 0.1568627450980392, 0.12156862745098039])
# print im2float([10, 20, 40, 31])
# print im2int(im2float([10, 20, 40, 31]))
# print psnr([10, 20, 40, 31], [10, 20, 40, 30], 2, 2)
# print mse([10, 20, 40, 31], [10, 20, 40, 30], 2, 2)
