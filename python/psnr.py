import scipy.misc
import numpy as np
import matplotlib.pyplot as plt
import argparse
import sys

from patch_1 import putback
from patch import patch
from disttool import *
import utilities as util

import matplotlib.pyplot as plt


parser = argparse.ArgumentParser(description='Simple SVM wrapper in python')
parser.add_argument('--encoded', help='path to encoded data')
parser.add_argument('--dictionary', help='path to dictionary')
parser.add_argument('--verbose', action='store_true', help='verbose flag')

args = parser.parse_args()
lena = scipy.misc.lena()
patch_size = [8, 8]

D = np.load(args.dictionary)
z = np.load(args.encoded)
B = np.dot(z, D)
Bt = B
Bt = Bt.reshape(-1, patch_size[0], patch_size[1])
x_p = putback(Bt, lena.shape, patch_size, sampling_method='slide')


x_p_int = im2int(x_p.flatten())
psnr_recon = psnr(x_p_int, lena.flatten(), 512, 512)


if args.verbose:
    print 'Recon psnr:', psnr_recon
else:
    sys.stdout.write(str('%2.3f, ' % psnr_recon))
