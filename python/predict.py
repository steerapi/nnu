import argparse
import numpy as np

import utilities as util

parser = argparse.ArgumentParser(description='Simple SVM wrapper in python')
parser.add_argument('--tr_data', help='training data text file')
parser.add_argument('--t_data', help='test data text file')
parser.add_argument('--tr_label', help='training label text file')
parser.add_argument('--t_label', help='test label text file')
parser.add_argument('--kernel', help='kernel type for svm', default='linear')
parser.add_argument('--verbose', action='store_true', help='verbose flag')
args = parser.parse_args()

tr_x = np.loadtxt(args.tr_data)
t_x = np.loadtxt(args.t_data)
tr_y = np.loadtxt(args.tr_label)
t_y = np.loadtxt(args.t_label)

if args.kernel == 'linear':
    util.predict_linear(tr_x, tr_y, t_x, t_y, False, args.verbose)
elif args.kernel == 'chi2':
    util.predict_chi2(tr_x, tr_y, t_x, t_y, False, args.verbose)
else:
    print 'Invalid kernel'
