import numpy as np
import glob
import patch
import argparse

import utilities as util
from zca import ZCA


def parse_cifar(inputfolder):
    train_data, test_data = [], []
    train_labels, test_labels = [], []

    batch_files = glob.glob(inputfolder + '*data_batch*')  #select numpy arrays
    for f in batch_files:
        np_file = np.load(f)
        train_data.append(np_file['data'])
        train_labels.append(np_file['labels'])

    batch_files = glob.glob(inputfolder + '*test_batch*')  #select numpy arrays
    for f in batch_files:
        np_file = np.load(f)
        test_data.append(np_file['data'])
        test_labels.append(np_file['labels'])

    train_data = np.vstack(train_data)
    train_labels = np.hstack(train_labels)
    test_data = np.vstack(test_data)
    test_labels = np.hstack(test_labels)

    return train_data, train_labels, test_data, test_labels



def patch_data(train_data, test_data, channel_size, patch_size, slide_size):
    train_patches = []
    zca_m = ZCA()
    for i in xrange(3):
        data = train_data[:4000, i*channel_size:(i+1)*channel_size].reshape(-1, 32, 32)
        patches = patch.patch(data, patch_size, slide_dims=slide_size,
                              sampling_method='slide')
        patches = np.array(patches)
        d1, d2, _, _ = patches.shape
        patches = patches.reshape(d1, d2, -1)
        train_patches.append(patches)


    train_patches = np.concatenate(train_patches, axis=2)
    d1, d2, _ = patches.shape
    train_patches = train_patches.reshape(d1*d2, -1)
    train_patches = train_patches.astype(np.float64)
    # train_patches = train_patches - np.mean(train_patches, axis=1)[:, np.newaxis]
    # non_zero_idxs = np.where(np.sum(train_patches, axis=1) != 0)[0]
    # train_patches[non_zero_idxs] = train_patches[non_zero_idxs] / \
                                   # np.linalg.norm(train_patches[non_zero_idxs],
                                                  # axis=1)[:, np.newaxis]

    zca_m.fit(train_patches[0:100000])
    train_patches = zca_m.transform(train_patches)
    train_patches = train_patches.reshape(d1, d2, -1)


    test_patches = []
    for i in xrange(3):
        data = test_data[:1000, i*channel_size:(i+1)*channel_size].reshape(-1, 32, 32)
        patches = patch.patch(data, patch_size, slide_dims=slide_size,
                              sampling_method='slide')
        patches = np.array(patches)
        d1, d2, _, _ = patches.shape
        patches = patches.reshape(d1, d2, -1)
        test_patches.append(patches)

    test_patches = np.concatenate(test_patches, axis=2)
    d1, d2, _ = patches.shape
    test_patches = test_patches.reshape(d1*d2, -1)
    test_patches = test_patches.astype(np.float64)
    # test_patches = test_patches - np.mean(test_patches, axis=1)[:, np.newaxis]
    # non_zero_idxs = np.where(np.sum(test_patches, axis=1) != 0)[0]
    # test_patches[non_zero_idxs] = test_patches[non_zero_idxs] / \
    #                                np.linalg.norm(test_patches[non_zero_idxs],
    #                                               axis=1)[:, np.newaxis]


    test_patches = zca_m.transform(test_patches)
    test_patches = test_patches.reshape(d1, d2, -1)


    return train_patches, test_patches



parser = argparse.ArgumentParser(description='Pipeline for cifar dataset')
parser.add_argument('--datafolder', help='folder to cifar-10 npz files')
parser.add_argument('--output', help='.npy filepath for learned centroids')
parser.add_argument('--inputfolder', help='Folder containing cifar-10 python' + \
                    ' data files', default='../data/cifar-10-batches-py/')
# parser.add_argument('--outputfolder', help='Output folder for parsed cifar-10',
#                     default='../data/cifar/')
parser.add_argument('--num_clusters', help='Number of clusters', type=int,
                    default=400)
parser.add_argument('--N_tr_size', help='Number of samples used to train the' + \
                    'dictionary', type=int, default=100000)
parser.add_argument('--omp_k', help='number of atoms used per signal', type=int,
                     default=5)
parser.add_argument('--patch', action='store_true', help='patches input and saves')
parser.add_argument('--trainD', action='store_true',
                    help='Trains a dictionary with patch data')
parser.add_argument('--encode', action='store_true',
                    help='encodes input with learned dictionary')
parser.add_argument('--predict', action='store_true',
                    help='runs predicition on encoded input')
parser.add_argument('--verbose', action='store_true', help='verbose flag')
parser.add_argument('--batch', action='store_true', help='batch mode flag')

args = parser.parse_args()



channel_size = 1024
training_lambda = 10
patch_size = [6, 6]
slide_size = [1, 1]


if args.patch:
    if args.verbose:
        print 'Patching'

    data = parse_cifar(args.inputfolder)
    train_data = data[0]
    train_labels = data[1]
    test_data = data[2]
    test_labels = data[3]

    train_patches, test_patches = patch_data(train_data, test_data, channel_size,
                                             patch_size, slide_size)


    with open('../data/cifar_train_labels.txt', 'w') as f:
        for i, t in enumerate(train_patches[0:4000]):
            f.write('train%05d.npy %d\n' % (i, train_labels[i]))

            np.save('../data/cifar_c/train%05d' % i,
                    np.ascontiguousarray(t, dtype=np.float16))


    with open('../data/cifar_test_labels.txt', 'w') as f:
        for i, t in enumerate(test_patches[0:1000]):
            f.write('test%05d.npy %d\n' % (i, test_labels[i]))

            np.save('../data/cifar_c/test%05d' % i,
                    np.ascontiguousarray(t, dtype=np.float16))





    # np.savez('../data/cifar/train', data=train_patches, labels=train_labels)
    # np.savez('../data/cifar/test', data=test_patches, labels=test_labels)


if args.trainD:
    if args.verbose:
        print 'Training Dictionary'

    train = np.load('../data/cifar/train.npz')
    train_data, train_labels = train['data'], train['labels']

    test = np.load('../data/cifar/test.npz')
    test_data, test_labels = test['data'], test['labels']

    atoms = args.num_clusters
    d1, d2, d3 = train_data.shape
    D_train_patches = train_data.reshape(d1*d2, -1)
    tr_idxs = np.random.permutation(len(D_train_patches))
    D = util.train_dict(D_train_patches[tr_idxs[:args.N_tr_size]], atoms,
                        training_lambda, verbose=args.verbose)

    d_name = '../results/cifar_D_cluster' + str(atoms) + '_tr_' +\
             str(args.N_tr_size)
    np.save(d_name, D)

    with open('../data/cifar_train_labels.txt', 'w') as f:
        for i, t in enumerate(train_data[0:8000]):
            f.write('train%5d.npy %d' % (i, train_labels[i]))

            np.save('../data/cifar_c/train%5d' % i,
                    np.ascontiguousarray(t, dtype=np.flat16))


    with open('../data/cifar_test_labels.txt', 'w') as f:
        for i, t in enumerate(test_data[0:1000]):
            f.write('test%5d.npy %d' % (i, test_labels[i]))

            np.save('../data/cifar_c/test%5d' % i,
                    np.ascontiguousarray(t, dtype=np.flat16))


if args.encode:
    if args.verbose:
        print 'Encoding'

    if not args.trainD:
        train = np.load('../data/cifar/train.npz')
        train_data, train_labels = train['data'], train['labels']

        test = np.load('../data/cifar/test.npz')
        test_data, test_labels = test['data'], test['labels']

        d_name = '../results/D_cluster' + str(args.num_clusters) + '_tr_' +\
                 str(args.N_tr_size) + '.npy'
        D = np.load(d_name)

    if args.verbose:
        print 'Encoding training'

    enc_train = []
    for i, t in enumerate(train_data[:4000]):
        if i % 10 == 0 and args.verbose:
            print i
        enc_sample = util.encode(t, D, args.omp_k)
        d1, d2 = enc_sample.shape
        d2_sr = np.sqrt(d2)
        region_sz = d2_sr / 2
        enc_sample = enc_sample.reshape(-1, d2_sr, d2_sr)
        q1 = np.max(enc_sample[:, :region_sz, :region_sz].reshape(d1, -1), axis=1) .flatten()
        q2 = np.max(enc_sample[:, region_sz:, :region_sz].reshape(d1, -1), axis=1) .flatten()
        q3 = np.max(enc_sample[:, :region_sz, region_sz:].reshape(d1, -1), axis=1) .flatten()
        q4 = np.max(enc_sample[:, region_sz:, region_sz:].reshape(d1, -1), axis=1) .flatten()
        # maxp_enc_sample = np.max(enc_sample, axis=1).flatten()
        enc_train.append(np.hstack((q1, q2, q3, q4)))
        # enc_train.append(maxp_enc_sample)

    if args.verbose:
        print 'Encoding testing'

    enc_test = []
    for i, t in enumerate(test_data[:1000]):
        if i % 10 == 0 and args.verbose:
            print i
        enc_sample = util.encode(t, D, args.omp_k)
        d1, d2 = enc_sample.shape
        d2_sr = np.sqrt(d2)
        region_sz = d2_sr / 2
        enc_sample = enc_sample.reshape(-1, d2_sr, d2_sr)
        q1 = np.max(enc_sample[:, :region_sz, :region_sz].reshape(d1, -1), axis=1) .flatten()
        q2 = np.max(enc_sample[:, region_sz:, :region_sz].reshape(d1, -1), axis=1) .flatten()
        q3 = np.max(enc_sample[:, :region_sz, region_sz:].reshape(d1, -1), axis=1) .flatten()
        q4 = np.max(enc_sample[:, region_sz:, region_sz:].reshape(d1, -1), axis=1) .flatten()
        # maxp_enc_sample = np.max(enc_sample, axis=1).flatten()
        enc_test.append(np.hstack((q1, q2, q3, q4)))
        # enc_test.append(maxp_enc_sample)

    if args.verbose:
        print 'Saving encoded data'


    enc_train = np.array(enc_train)
    enc_test = np.array(enc_test)

    enc_train_fname = '../results/enc_D' + str(args.num_clusters) + '_k' +\
                      str(args.omp_k) + 'train'
    np.save(enc_train_fname, enc_train)

    enc_test_fname = '../results/enc_D' + str(args.num_clusters) + '_k' +\
                     str(args.omp_k) + 'test'
    np.save(enc_test_fname, enc_test)


if args.predict:
    if args.verbose:
        print 'Classification'

    if not args.encode:
        enc_train_fname = '../results/enc_D' + str(args.num_clusters) + '_k' +\
                          str(args.omp_k) + 'train.npy'

        enc_test_fname = '../results/enc_D' + str(args.num_clusters) + '_k' +\
                          str(args.omp_k) + 'test.npy'

        enc_train = np.load(enc_train_fname)
        enc_test = np.load(enc_test_fname)
        train = np.load('../data/cifar/train.npz')
        _, train_labels = train['data'], train['labels']
        test = np.load('../data/cifar/test.npz')
        _, test_labels = test['data'], test['labels']

    util.predict_linear(enc_train, train_labels[:4000], enc_test,
                        test_labels[:1000], batch=args.batch, verbose=args.verbose)
