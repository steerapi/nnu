'''Simple command line interface around sklearn minibatch kmeans'''

import numpy as np
from sklearn.cluster import MiniBatchKMeans
import argparse

import utilities as util

parser = argparse.ArgumentParser(description='Wrapper around sklearn KMeans')
parser.add_argument('--num_clusters', help='Number of clusters', type=int)
parser.add_argument('--method', help='clustering method')
parser.add_argument('--data', help='.npy filepath of size: examples x dims')
parser.add_argument('--k', help='.npy filepath of size: examples x dims', type=int)
parser.add_argument('--output', help='.npy filepath for learned centroids')

args = parser.parse_args()
data = np.load(args.data)

if args.method == 'KM':
    KM = MiniBatchKMeans(args.num_clusters, batch_size=min(len(data), 2000))
    KM.fit(data)
    np.save(args.output, KM.cluster_centers_)
elif args.method == 'KSVD':
    D = util.train_dict(data, args.num_clusters, args.k).T
    np.save(args.output, D)
