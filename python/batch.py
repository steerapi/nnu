import subprocess
import argparse
from itertools import product


def run_command(cmd_str):
    output = subprocess.check_output(cmd_str, shell=True)
    return output.rstrip()

parser = argparse.ArgumentParser(description='Batch jobs for run_application')
parser.add_argument('-z', '--result_file', help='output path for batch job', required=True)
parser.add_argument('-i', '--bin_folder', help='path to exe folder', required=True)
parser.add_argument('-u', '--script', help='script to run', default='run_application',
                    choices=['run_application', 'run_num_correct'])
parser.add_argument('-p', '--application', help='application',
                    choices=['kth', 'cifar', 'lena'], required=True)
parser.add_argument('-e', '--encoders', nargs='+', help='list of encoders',
                    required=True)
parser.add_argument('-s', '--svm_kernel', help='svm kernel type',
                    choices=['linear', 'chi2'], required=True)
parser.add_argument('-d', '--data_folder', help='data folder path', required=True)
parser.add_argument('-r', '--train_file', help='training file path', required=True)
parser.add_argument('-t', '--test_file', help='training file path', default='')
parser.add_argument('-c', '--cluster_method', help='Method used for learning dictionary',
                    choices=['KM', 'KSVD'], required=True)
parser.add_argument('-y', '--python_path', help='path to python files', required=True)
parser.add_argument('-w', '--num_atoms', help='# of dictionary atoms',
                    default=1000)
parser.add_argument('-q', '--num_tr_samples',help='# of samples used for training D',
                    default=10000)
parser.add_argument('-a', '--alpha', nargs='+', help='nnu alpha (or #PCs for non-nnu algorithms)',
                    required=True)
parser.add_argument('-b', '--beta', nargs='+', help='nnu beta (ignored for non-nnu algorithms)',
                    required=True)
parser.add_argument('-k', '--mp_iters', help='# of iterations for mp', default='1')
parser.add_argument('-v', '--verbose', action='store_true', help='verbose flag')
args = parser.parse_args()


arg_order = ['-p', '-e', '-s', '-d', '-r', '-t', '-c', '-y', '-w', '-q', '-a', '-b', '-k']
cmd_strs = product([args.application], args.encoders, [args.svm_kernel], [args.data_folder],
                   [args.train_file], [args.test_file], [args.cluster_method],
                   [args.python_path], [args.num_atoms], [args.num_tr_samples], args.alpha,
                   args.beta, [args.mp_iters])
cmd_strs = list(cmd_strs)

for cmd_str in cmd_strs:
    cmd_str = [' '.join(tup) for tup in zip(arg_order, cmd_str)]
    cmd_str = './' + args.bin_folder + args.script + ' ' + ' '.join(cmd_str)
    if args.test_file == '':
        cmd_str = cmd_str.replace(' -t ', '')

    print 'Running:'
    print cmd_str
    print ''
    result_str = run_command(cmd_str)

    with open(args.result_file, 'a') as file:
        file.write(result_str + '\n')
