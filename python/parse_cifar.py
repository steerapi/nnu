#Short script to parse cifar data into patches for pipeline
import os

import numpy as np
import glob
import argparse

parser = argparse.ArgumentParser(description='Parses/normalizes Cifar data')
parser.add_argument('--inputfolder', help='Folder containing cifar-10 python' + \
                    ' data files', default='../data/cifar-10-batches-py/')
parser.add_argument('--outputfolder', help='Output folder for parsed cifar-10',
                    default='../data/cifar/')


args = parser.parse_args()
channel_size = 1024

if not os.path.exists(args.outputfolder):
    os.makedirs(args.outputfolder)

batch_files = glob.glob(args.inputfolder + '*_batch*')  #select numpy arrays

for f in batch_files:
    #load data
    fname = f.split('/')[-1]
    np_file = np.load(f)
    data = np_file['data']

    #conver to greyscale
    data = rgb2grey(data, channel_size)

    #normalize data
    data = data - np.mean(data, axis=0)
    data = data / np.linalg.norm(data, axis=1)[:, np.newaxis]
    data = np.ascontiguousarray(data)

    #get labels
    labels = np.array(np_file['labels'])

    np.savez(args.outputfolder + fname, data=data, labels=labels)







