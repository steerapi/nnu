import numpy as np
from scipy.spatial.distance import pdist

def avgdist(D):
#     return np.mean(D.T.dot(D));
    return np.mean(pdist(D,'cosine'));

from scipy.misc import toimage

def show_dict(comps,shapeIm=[28,28],shapeSp=[4,4],shapeDict=None):
    blank = image_dict(comps,shapeIm,shapeSp,shapeDict)
    toimage(blank).show()
    return
    
def image_dict(comps,shapeIm=[28,28],shapeSp=[4,4],shapeDict=None):
    total = len(comps)
    if shapeDict==None:
        shapeDict = [np.ceil(np.sqrt(total)).astype(int),np.ceil(np.sqrt(total)).astype(int)]
    images = np.array([np.reshape(comp,shapeIm) for comp in comps])
    interval = (np.array(shapeIm)+np.array(shapeSp))
    blank = np.zeros(interval*np.array(shapeDict))
    k = 0
    for i in range(shapeDict[0]):
        for j in range(shapeDict[1]):
            if k < total:
                blank[i*interval[0]:i*interval[0]+shapeIm[0],j*interval[1]:j*interval[1]+shapeIm[1]] = images[k]
                k+=1
    return blank