import scipy.misc
import numpy as np
import matplotlib.pyplot as plt
import argparse

from patch_1 import putback
from patch import patch
from disttool import *
import utilities as util


parser = argparse.ArgumentParser(description='Simple SVM wrapper in python')
parser.add_argument('--patch', action='store_true', help='patches lena')
parser.add_argument('--patch_size', type=int, help='patches lena',
                    default=8)
parser.add_argument('--verbose', action='store_true', help='verbose flag')

args = parser.parse_args()


lena = scipy.misc.lena()
lena_clean = np.array(im2float(lena.flatten()))
noise_std = 0.05
patch_size = [8, 8]

lena_noise = np.clip(lena_clean + np.random.normal(0, noise_std,
                     len(lena_clean)), 0, 1)
lena_clean = lena_clean.reshape(512, 512)
lena_noise = lena_noise.reshape(512, 512)
# np.save('../results/lena_noise', lena_noise)


lena_patches = patch([lena_noise], patch_size, sampling_method='slide')

lena_patches = np.array(lena_patches)[0]

d1, _, _ = lena_patches.shape
lena_patches = lena_patches.reshape(d1, -1)

D = util.train_dict(lena_patches, 3000, 5, verbose=True)

# d_name = '../results/lena_D_cluster' + str(1000) + '_tr_' +\
#          str(100000)
# np.save(d_name, D)

# np.save('../data/lena', lena_patches)
import utils
utils.show_dict(D.T, shapeIm=[8, 8])

z = util.encode(lena_patches, D, 5)
B = np.dot(D, z)
Bt = B.T
Bt = Bt.reshape(-1, patch_size[0], patch_size[1])
x_p = putback(Bt, lena.shape, patch_size, sampling_method='slide')

x_p_int = im2int(x_p.flatten())
lena_noise_int = im2int(lena_noise.flatten())
psnr_orig = psnr(lena_noise_int, lena.flatten(), 512, 512)
psnr_recon = psnr(x_p_int, lena.flatten(), 512, 512)

print 'Noise psnr:', psnr_orig
print 'Recon psnr:', psnr_recon


plt.gray()
plt.subplot(221)
plt.imshow(lena_noise)
plt.subplot(222)
plt.imshow(x_p)
plt.show()
