from functools import partial
from multiprocessing import Pool

import numpy as np
import random

def _product(*iterables, **kwargs):
    '''Brad's memoryless implementation of itertools.product'''
    if len(iterables) == 0:
        yield ()
    else:
        iterables = iterables * kwargs.get('repeat', 1)
        it = iterables[0]
        for item in it() if callable(it) else iter(it):
            for items in _product(*iterables[1:]):
                yield (item, ) + items


def iter_sample_fast(iterable, samplesize):
    '''Samples from iterable -- from
       http://stackoverflow.com/questions/12581437/
       python-random-sample-with-a-generator
    '''
    results = []
    iterator = iter(iterable)

    # Fill in the first samplesize elements:
    for _ in xrange(samplesize):
        results.append(iterator.next())
    random.shuffle(results)  # Randomize their positions
    for i, v in enumerate(iterator, samplesize):
        r = random.randint(0, i)
        if r < samplesize:
            results[r] = v  # at a decreasing rate, replace random items

    if len(results) < samplesize:
        raise ValueError("Sample larger than population.")
    return results


def random_sample(x, patch_dims, N, slide_dims=None):
    '''randomly samples N patches of size patch_dims from x'''
    if not slide_dims:
        slide_dims = [max(1, patch_dim/2) for patch_dim in patch_dims]

    gen = islices(x, patch_dims, slide_dims)

    return np.array(iter_sample_fast(gen, N))


def slices(x, patch_dims, slide_dims=None):
    '''wrapper around islices'''
    return np.array(list(islices(x, patch_dims, slide_dims)))


def islices(x, patch_dims, slide_dims=None):
    '''Generator for slicing'''
    if not slide_dims:
        slide_dims = [1] * len(patch_dims)

    dims = x.shape

    product_list = []
    for dim, patch_dim, slide_dim in zip(dims, patch_dims, slide_dims):
        product_list.append(xrange(0, dim - patch_dim + 1,
                                   slide_dim))


    start_idx_gen = _product(*product_list)

    for start_idx in start_idx_gen:
        slices = [slice(start, start+end)
                  for start, end in zip(start_idx, patch_dims)]

        yield x[slices]


def unslice(x, dims, patch_dims, slide_dims=None):
    if not slide_dims:
        slide_dims = [1] * len(patch_dims)

    for dim, patch_dim, slide_dim in zip(dims, patch_dims, slide_dims):
        product_list.append(xrange(0, dim - patch_dim + 1,
                                   slide_dim))

    start_idx_gen = _product(*product_list)

    for start_idx in start_idx_gen:
        pass





def patch(xs, patch_dims, sampling_method='random', N=100,
          slide_dims=None):
    '''n-dimensional patching function
       xs is an n-dimensional array of data,
       the first dimension denotes the number
       of samples, the rest of the dimensions
       denotes the shape of each sample.

       patch_dims is the dimensions of the feature
       to extract.

       sampling_method is the method used to extract
       the patches.
       'random' randomly samples N patches for each input
       'slide' takes sliding patches in each dimension and
       requires slide_patch_dims to denote the stride of each slide
       'block' is a special case of sliding with the sliding_patch_dims
       equal to the patch_dims

        N is the number of random samples per sample for 'random'

        slide_dims is the sliding dimensions for 'slide'

    '''

    if not sampling_method in ['random', 'slide', 'block']:
        print 'Invalid sampling method'
        assert False


    if sampling_method is 'random':
        f = partial(random_sample, N=N, patch_dims=patch_dims)

    elif sampling_method is 'slide':
        if slide_dims is None:
            slide_dims = [1]*len(xs[0].shape)

        f = partial(slices, patch_dims=patch_dims,
                    slide_dims=slide_dims)

    elif sampling_method is 'block':
        slide_dims = patch_dims
        f = partial(slices, patch_dims=patch_dims,
                    slide_dims=slide_dims)


    # p = Pool(processes=None, maxtasksperchild=1)
    patches = []
    for i, x in enumerate(xs):
        if i % 100 == 0:
            print i
        patches.append(f(x))

    # patches = [f(x) for x in xs]
    # patches = p.map(f, xs)

    # p.terminate()
    # p.join()

    return patches



if __name__=='__main__':
    samples = np.zeros((1, 500, 160, 120))
    patch_dims = [16, 16, 16]
    slide_dims = [8, 8, 8]

    # print '{} samples with shape: {}'.format(samples.shape[0],
    #                                          samples.shape[1:])

    # print 'Using patch dims: ', patch_dims
    # patches = patch(samples, patch_dims, sampling_method='random',
    #                 N=100, num_cores=-1, verbose=5)
    # print patches.shape

    print 'With slide sampling moving 1 in each direction'
    patches = patch(samples, patch_dims, sampling_method='slide',
                    slide_dims=slide_dims, num_cores=-1, verbose=5)
    print patches.shape

    print 'With block sampling'
    patches = patch(samples, patch_dims, sampling_method='block')
    print patches.shape
