from functools import partial
import sys

import argparse
import numpy as np
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.metrics.pairwise import chi2_kernel


parser = argparse.ArgumentParser(description='Simple SVM wrapper in python')
parser.add_argument('--tr_data', help='training data text file')
parser.add_argument('--t_data', help='test data text file')
parser.add_argument('--tr_label', help='training label text file')
parser.add_argument('--t_label', help='test label text file')
parser.add_argument('--verbose', action='store_true', help='verbose flag')

args = parser.parse_args()


tr_x = np.loadtxt(args.tr_data)
t_x = np.loadtxt(args.t_data)
tr_y = np.loadtxt(args.tr_label)
t_y = np.loadtxt(args.t_label)


max_acc = 0
tr_acc = 0
cm = None
tr_cm = None
tuned_c, tuned_gamma = None, None

#screen of gamma and C
for g in [0.01, 0.05, .1]:
    for C in [1, 10, 100]:
        clf = SVC(kernel=partial(chi2_kernel, gamma=g), C=C)
        clf.fit(tr_x, tr_y)
        y_pred = clf.predict(t_x)
        new_acc = accuracy_score(t_y, y_pred)

        if new_acc > max_acc:
            tr_y_pred = clf.predict(tr_x)
            tr_acc = accuracy_score(tr_y, tr_y_pred)
            tr_cm = confusion_matrix(tr_y, tr_y_pred)
            max_acc = new_acc
            cm = confusion_matrix(t_y, y_pred)
            tuned_c = C
            tuned_gamma = g

if args.verbose:
    print 'gamma, C: ', tuned_gamma, tuned_c
    print 'Training'
    print 'Accuracy:', tr_acc
    print tr_cm
    print ''

    print 'Testing'

sys.stdout.write(str('%2.2f, ' % max_acc))

if args.verbose:
    print cm
