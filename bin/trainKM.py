'''Simple command line interface around sklearn minibatch kmeans'''

import numpy as np
from sklearn.cluster import MiniBatchKMeans
import argparse
import os


parser = argparse.ArgumentParser(description='Wrapper around sklearn KMeans')
parser.add_argument('--num_clusters', help='Number of clusters', type=int)
parser.add_argument('--data', help='.npy filepath of size: examples x dims')
parser.add_argument('--output', help='.npy filepath for learned centroids')

args = parser.parse_args()

data = np.load(args.data)
KM = MiniBatchKMeans(args.num_clusters, batch_size=min(len(data), 2000))
KM.fit(data)
np.save(args.output, np.ascontiguousarray(KM.cluster_centers_, dtype=np.float64))
