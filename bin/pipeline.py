import numpy as np
from sklearn.cluster import MiniBatchKMeans
import argparse
import os
import glob
from joblib import Parallel, delayed
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.metrics.pairwise import chi2_kernel, euclidean_distances

from functools import partial
import time

total_time = 0.0


def compute_bow(xs, N):
    xs_rep = []

    for item in xs:
        xs_rep.append(bag_rep(item, minlength=N))

    xs_rep = np.array(xs_rep)

    return xs_rep


def bag_rep(xs, minlength):
    counts = np.bincount(xs, minlength=minlength)
    return counts/np.linalg.norm(counts)



def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass

    return i + 1




def extract_data(data_folder, label_file, num_samples):

    num_files = file_len(label_file)
    max_rows_per_file = num_samples / num_files
    total_rows = 0


    with open(label_file, 'r') as f:
        for line in f:
            line = line.rstrip().split(' ')[0]
            f_data = np.load(data_folder + line)
            num_features = f_data.shape[1]
            total_rows += min(f_data.shape[0], max_rows_per_file)

    data = np.zeros((total_rows, num_features))
    idx = 0

    with open(label_file, 'r') as f:
        for line in f:
            fname = line.split(' ')[0]
            f_data = np.load(data_folder + fname)

            for i in xrange(len(f_data)):
                if i < max_rows_per_file:
                    data[idx] = f_data[i]
                    idx += 1
                else:
                    break

    print total_rows, idx
    return data



def encode_bow(fname, KM, K_clusters, data_folder):
    global total_time
    data = np.load(fname).astype(np.float64)
    start_t = time.time()
    # enc_data = KM.predict(data)
    total_time += (time.time() - start_t)
    norm_data = data / np.linalg.norm(data, axis=1)[:, np.newaxis]
    norm_clusters = KM.cluster_centers_ / np.linalg.norm(KM.cluster_centers_,
                                                         axis=1)[:, np.newaxis]
    enc_data = np.dot(norm_clusters, norm_data.T)
    enc_data = np.argmax(enc_data, axis=0)

    bow_enc = bag_rep(enc_data, minlength=K_clusters)

    return bow_enc

def get_xs_ys(data_folder, label_file, KM, K_clusters):
    ys = []
    fnames = []

    with open(label_file, 'r') as f:
        for line in f:
             line = line.rstrip()
             fname = line.split(' ')[0]
             y = int(line.split(' ')[1])
             ys.append(y)
             fnames.append(data_folder + fname)


    # p = Parallel(n_jobs=16, verbose=5, pre_dispatch=8)
    # xs = p(delayed(encode_bow)(fname, KM, K_clusters) for fname in fnames)
    xs = []
    for fname in fnames:
        x = encode_bow(fname, KM, K_clusters, data_folder)
        xs.append(x)
        # if fname == data_folder + 'S01_boxing_1.npy':
        #     print np.sum(x)
        #     assert False

    return xs, ys



parser = argparse.ArgumentParser(description='action_recognition pipeline')
parser.add_argument('--num_clusters', help='Number of clusters', type=int)
parser.add_argument('--KM_tr_data', help='number of samples to train KMeans',
                    type=int)
parser.add_argument('--data_folder', help='folder of .npy feature files')
parser.add_argument('--tr_label_file', help='label file for training')
parser.add_argument('--t_label_file', help='label file for testing')


args = parser.parse_args()

data_tr = extract_data(args.data_folder, args.tr_label_file, args.KM_tr_data)
KM = MiniBatchKMeans(args.num_clusters, batch_size=min(len(data_tr), 2000))
KM.fit(data_tr)
np.save('out', KM.cluster_centers_)
# KM.cluster_centers_ = np.load('out.npy')


print 'Encoding training'
tr_x, tr_y = get_xs_ys(args.data_folder, args.tr_label_file, KM, args.num_clusters)

print 'Encoding testing'
t_x, t_y = get_xs_ys(args.data_folder, args.t_label_file, KM, args.num_clusters)


max_acc = 0
tr_acc = 0
cm = None
tr_cm = None
tuned_c, tuned_gamma = None, None

#screen of gamma and C
for g in [0.01, 0.05, .1]:
    for C in [1, 10, 100]:
        print g, C
        clf = SVC(kernel=partial(chi2_kernel, gamma=g), C=C)
        clf.fit(tr_x, tr_y)
        y_pred = clf.predict(t_x)
        new_acc = accuracy_score(t_y, y_pred)

        if new_acc > max_acc:
            tr_y_pred = clf.predict(tr_x)
            tr_acc = accuracy_score(tr_y, tr_y_pred)
            tr_cm = confusion_matrix(tr_y, tr_y_pred)
            max_acc = new_acc
            cm = confusion_matrix(t_y, y_pred)
            tuned_c = C
            tuned_gamma = g


print 'gamma, C: ', tuned_gamma, tuned_c
print 'Training'
print 'Accuracy:', tr_acc
print tr_cm
print ''

print 'Testing'
print 'Accuracy:', max_acc
print cm
print total_time
