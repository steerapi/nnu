/*
 * hashlist.hpp
 *
 *  Created on: Apr 19, 2015
 *      Author: surat
 */

#ifndef SRC_HASHLIST_HPP_
#define SRC_HASHLIST_HPP_

#include <unistd.h>
#include <mutex>
#include <vector>
#include <atomic>

using namespace std;

template<class T> class HashList {
public:
	atomic<T> C;
	atomic<bool> *flags;
	atomic<T>* links;
	T size;
	mutex c_mutex;

	HashList(T num) :
			size(num) {
		C = 0;
		links = new atomic<T> [num];
		flags = new atomic<bool> [num];
		for (int i = 0; i < num; ++i) {
			links[i] = num;
			flags[i] = false;
		}
	}
	void insert(T v) {
//		if (flags[v]) {
//			return;
//		}
		T tst;
		do {
			tst = size;
			T c = C;
			while (links[c] != size) {
				c = links[c];
				cout << "he" << endl;
			}
			C = c;
//			cout << "ho" << endl;
		} while (!(links[C].compare_exchange_weak(tst, v)));
		// If exchanged

		C = v;
//		if (flags[v]) {
//			return;
//		}
//		T tst = size;
//		T c = C;
//		check: if (links[c] != size) {
//			//Other people took it already, wait until c is changed
//			return;
//		}
//		tst = size;
//		if (links[c].compare_exchange_strong(tst, v)) {
//			// If exchanged
//			C = v;
//		} else {
//			c = C;
//			goto check;
//		}
	}
	vector<T> get() {
		vector<T> list;
		T v = links[0];
		while (v != 2000) {
//			cout << "v" << v << "\n";
			list.push_back(v);
			v = links[v];
		}
		return list;
	}
	~HashList() {
		delete[] links;
		delete[] flags;
	}
};

#endif /* SRC_HASHLIST_HPP_ */
