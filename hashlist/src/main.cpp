//============================================================================
// Name        : main.cpp
// Author      : Surat Teerapittayanon
// Version     : 1.0
//============================================================================

#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <thread>
#include <vector>

#include "hashlist.hpp"
#include "hashtable.hpp"
#include "lockset.hpp"
#include "tictoc.hpp"

using namespace std;

void print(vector<uint64_t> vec) {
	for (vector<uint64_t>::const_iterator i = vec.begin(); i != vec.end();
			++i) {
		cout << *i << ' ';
	}
	cout << "\n";
}

int main() {
	srand(1);
	tic();
	for (int var = 0; var < 100; ++var) {
//		LockSet hashlist;
		HashList<uint64_t> hashlist(2000);
//		HashTable hashtable;

		vector<thread> threads;

//		tic();
//		tic();
		//	Create N threads
		for (int i = 0; i < 32; ++i) {
//			int a = floor(1.0 * rand() / RAND_MAX * 2000);
			threads.push_back(thread([&] {
				for (int j = 0; j < 10; ++j) {
					int a = floor(1.0 * rand() / RAND_MAX * 2000);
					hashlist.insert(a);
				}
			}));
		}
//		cout << toc() << "\n";
		// Synchronize threads:
		for (auto &thread : threads) {
			thread.join();
		}
		print(hashlist.get());
//		cout << toc() << "\n";

	}
	cout << toc() << "\n";
	return 0;
}
