/*
 * lockset.hpp
 *
 *  Created on: Apr 19, 2015
 *      Author: surat
 */

#ifndef SRC_LOCKSET_HPP_
#define SRC_LOCKSET_HPP_

#include <cstdint>
#include <set>
#include <vector>
#include <mutex>

using namespace std;

class LockSet {
public:
	set<uint64_t> lockset;
	mutex g_pages_mutex;

	LockSet() {

	}
	void insert(uint64_t v) {
		g_pages_mutex.lock();
		lockset.insert(v);
		g_pages_mutex.unlock();
	}
	vector<uint64_t> get() {
		return vector<uint64_t>(lockset.begin(), lockset.end());
	}
	~LockSet() {

	}
};

#endif /* SRC_LOCKSET_HPP_ */
