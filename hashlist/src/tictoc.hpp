/*
 * tictoc.hpp
 *
 *  Created on: Oct 29, 2014
 *      Author: Au
 */

#ifndef TICTOC_HPP_
#define TICTOC_HPP_

#include <stack>
#include <ctime>

std::stack<clock_t> tictoc_stack;

void tic() {
	tictoc_stack.push(clock());
}

double toc() {
	double time = ((double) (clock() - tictoc_stack.top())) / CLOCKS_PER_SEC;
	tictoc_stack.pop();
	return time;
}







#endif /* TICTOC_HPP_ */
