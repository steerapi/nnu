/*
 * hashlist.hpp
 *
 *  Created on: Apr 19, 2015
 *      Author: surat
 */

#ifndef SRC_HASHLIST_HPP_
#define SRC_HASHLIST_HPP_

#include <mutex>
#include <cstdint>
#include <set>
#include <vector>
#include <atomic>

using namespace std;

template<class T> class HashList {
public:
	atomic<T> C;
	atomic<bool> *flags;
	atomic<T>* links;
	T size;
	mutex c_mutex;

	HashList(T num) :
			size(num) {
		C = 0;
		links = new atomic<T> [num];
		flags = new atomic<bool> [num];
		for (int i = 0; i < num; ++i) {
			links[i] = num;
			flags[i] = false;
		}
	}
	void insert(T v) {
		c_mutex.lock();
		if (flags[v]) {
			c_mutex.unlock();
			return;
		}
		T tst = size;
		T c = C.load(memory_order::memory_order_relaxed);
		while(!(links[c].compare_exchange_weak(tst, v, memory_order::memory_order_relaxed))){
			tst = size;
			c = C.load(memory_order::memory_order_relaxed);
			if(links[c]!=size){
				//Other people took it already
				break;
			}
//			cout << "test: " << tst << "|" << links[c].load() <<"|" << "\n";
		}
		// If exchanged
		C.store(v,memory_order::memory_order_relaxed);
		flags[v] = true;
		c_mutex.unlock();
	}
	vector<T> get() {
		vector<T> list;
		T v = links[0];
		while (v != 2000) {
//			cout << "v" << v << "\n";
			list.push_back(v);
			v = links[v];
		}
		return list;
	}
	~HashList() {
		delete[] links;
		delete[] flags;
	}
};

#endif /* SRC_HASHLIST_HPP_ */
