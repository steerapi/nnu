/*
 * hashtable.hpp
 *
 *  Created on: Apr 19, 2015
 *      Author: surat
 */

#ifndef SRC_HASHTABLE_HPP_
#define SRC_HASHTABLE_HPP_

#include <mutex>
#include <cstdint>
#include <set>
#include <vector>
#include <atomic>

using namespace std;

template<class T>
class HashTable {
public:
	atomic<bool> *table;
	bool tst = false;
	T size;

	HashTable(T num) :
			size(num) {
		table = new atomic<bool> [num];
		for (int i = 0; i < size; ++i) {
			table[i] = false;
		}
	}
	void insert(T v) {
		table[v] = true;

//		table[v].compare_exchange_strong(tst, true);
	}
	vector<T> get() {
		vector<T> list;
		T v = table[0];
		for (int i = 0; i < size; ++i) {
			if (table[i]) {
				list.push_back(i);
			}
		}
		return list;
	}
	~HashTable() {
		delete[] table;
	}
};

#endif /* SRC_HASHTABLE_HPP_ */
