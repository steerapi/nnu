#ifndef STRUCTS_CPP_
#define STRUCTS_CPP_
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cmath>
#include <vector>
#include <set>
#include <mutex>
#include <thread>
#include <sstream>
#include <sys/time.h>
#include <sys/stat.h>

/* #include "sortindex.hpp" */
#include <Eigen/Dense>
#include "eigenio.hpp"
#include "structs.h"

using namespace Eigen;

/* associates class label with filename */
struct label {
	string fname;
	int class_id;
	label(string Fname, int Class_id) {
		fname = Fname;
		class_id = Class_id;
	}

};

std::vector<label> extract_labels(string file);

/* holds label and data */
struct label_data {
	int class_id;
	vector<double> data;
	MatrixXd mat_data; //used for lena
	label_data(int Class_id, vector<double> Data) {
		class_id = Class_id;
		data = Data;
	}

	label_data(int Class_id, Eigen::VectorXd Data) {
		class_id = Class_id;
		data.resize(Data.size());
		VectorXd::Map(&data[0], Data.size()) = Data;
	}

	label_data(int Class_id, MatrixXd Mat_data) {
		class_id = Class_id;
		mat_data = Mat_data;
	}
};

double get_wall_time() {
	struct timeval time;
	if (gettimeofday(&time, NULL)) {
		return 0;
	}
	return (double) time.tv_sec + (double) time.tv_usec * .000001;
}

inline bool file_exists(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

void save_label_data(vector<label_data> data, string label_path,
		string data_path) {
	ofstream label_file;
	ofstream data_file;
	label_file.open(label_path.c_str());
	data_file.open(data_path.c_str());
	for (int i = 0; i < data.size(); i++) {
		for (int j = 0; j < data[i].data.size(); j++) {
			data_file << data[i].data[j] << ' ';
		}

		data_file << '\n';
		label_file << data[i].class_id << '\n';
	}

	label_file.close();
	data_file.close();
}

double pct_match(VectorXd a, VectorXd b) {
	int match = 0;
	for (int i = 0; i < a.size(); i++) {
		if (a(i) == b(i)) {
			match++;
		}
	}

	return (float) match / a.size();
}

//gets the dimension of X in a data folder
int get_Xdim(string data_folder, string label_file) {
	vector<label> labels = extract_labels(label_file);
	MatrixXd x;
	EigenIO::read_npy_half((data_folder + labels[0].fname).c_str(), x);
	return x.rows();
}

MatrixXd extract_data(string data_folder, string label_file, int num_data,
		bool verbose) {
	vector<label> labels = extract_labels(label_file);

	MatrixXd x;
	MatrixXd data;
	int idx = 0;
	int num_files = labels.size();
	int items_per_file = num_data / num_files;
	int r;
	double pct_done = 0.01;

	if (verbose)
		cout << "Extracting data from files.\n";

	for (int i = 0; i < labels.size(); i++) {
		EigenIO::read_npy_half((data_folder + labels[i].fname).c_str(), x);

		if (i == 0) {
			data = MatrixXd::Zero(num_data, x.rows());
		}

		if (x.cols() < items_per_file) {
			for (int j = 0; j < x.cols(); j++) {
				data.row(idx) = x.col(j);
				idx++;
			}
		} else {
			//add randomly selected data to KM_data
			std::vector<int> rands;
			for (int j = 0; j < items_per_file; j++) {
				r = rand() % x.cols();
				while (std::find(rands.begin(), rands.end(), r) != rands.end()) {
					r = rand() % x.cols();
				}
				rands.push_back(r);
				data.row(idx) = x.col(r);
				idx++;
			}
		}

		if (verbose && (float) i / num_files > pct_done) {
			cout << '\r' << pct_done * 100 << "%" << flush;
			pct_done += 0.01;
		}

	}

	if (verbose)
		cout << endl;

	return data;

}

MatrixXd pairwise_euclidean(MatrixXd& p0, MatrixXd& p1) {
	MatrixXd D(p0.cols(), p0.rows());
	for (int i = 0; i < p1.cols(); i++)
		D.col(i) =
				(p0.colwise() - p1.col(i)).colwise().squaredNorm().transpose();

	return D;
}

void l2_norm(MatrixXd& input) {
	input = input.array().rowwise() / input.colwise().norm().eval().array();
}

VectorXd pool_all(MatrixXd X) {
	return X.colwise().maxCoeff();
}

VectorXd convert_to_bow(VectorXd x, int num_components) {
	VectorXd count_rep = Eigen::VectorXd::Zero(num_components);
	for (int i = 0; i < x.size(); i++) {
		count_rep(x(i))++;}

	return count_rep / count_rep.norm();
}

vector<label> extract_labels(string file) {
	ifstream infile(file);
	vector<label> labels;
	string fname;
	int class_idx;

	if (!infile) {
		cout << file << " does not exist.\n";
		exit(2);
	}

	while (infile >> fname >> class_idx) {
		labels.push_back(label(fname, class_idx));
	}

	return labels;
}

int comp_num_multadd_nns(int atoms, int dim_x) {
	return atoms * (2 * dim_x - 1);
}

int comp_num_multadd_nnu(int alpha, int beta, int dim_x) {
	return alpha * beta * (2 * dim_x - 1);
}

#endif
