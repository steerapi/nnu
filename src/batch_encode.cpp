#ifndef BATCH_ENCODE_CPP_
#define BATCH_ENCODE_CPP_

#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <glob.h>
#include <sys/mman.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <sys/stat.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "nnudictionary.hpp"
#include "nnumpcode.hpp"
#include "tictoc.hpp"
#include "eigenio.hpp"
#include "utilities.cpp"
#include "pca.hpp"

using namespace std;
void threshold_small(VectorXd&);
void threshold_small(MatrixXd&);

/*Returns the percentage of correctly matched d*/
double bow_encode_pct_correct(NNU::Dictionary* NNU_D, string label_file,
                              cmd_arguments args, MatrixXd& vappt) {
    vector<label> labels = extract_labels(label_file);
    vector<label_data> datum;
    double pct_done = 0.01;
    MatrixXd D = NNU_D->get_D();
    static MatrixXd vapptD = vappt * D;
    MatrixXd x;
    MatrixXd z;
    MatrixXd correct_z;
    VectorXd v;
    int num_correct = 0;
    int num_total = 0;

    /* for (int i = 0; i < labels.size(); i++) { */
    for (int i = 0; i < 20; i++) {
        EigenIO::read_npy_half((args.data_folder + labels[i].fname).c_str(), x);

        l2_norm(x);

        correct_z = NNU::nns(NNU_D, vappt, x, D);

        //perform encoding
        if (args.encoder == "nns") {
            z = NNU::nns(NNU_D, vappt, x, D);
        } else if (args.encoder == "par-nns") {
            z = NNU::par_nns(NNU_D, vappt, x, D);
        } else if (args.encoder == "nns-pca-x") {
            z = NNU::nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "par-nns-pca-x") {
            z = NNU::par_nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "nns-pca-d") {
            z = NNU::nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "par-nns-pca-d") {
            z = NNU::par_nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "nnu") {
            z = NNU::nnu(NNU_D, vappt, x, D);
        } else if (args.encoder == "par-nnu") {
            z = NNU::par_nnu(NNU_D, vappt, x, D);
        } else if (args.encoder == "nnu-pca") {
            z = NNU::nnu_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "par-nnu-pca") {
            z = NNU::par_nnu_pca(NNU_D, vappt, vapptD, x, D);
        } else {
            cout << "Invalid encoder for BoW.\n";
            exit(1);
        }

        for (int j = 0; j < z.size(); ++j) {
            if (z(j) == correct_z(j)) 
                num_correct++;

            num_total++;
        }

        /* if (args.verbose && (float) i / labels.size() > pct_done) { */
        /*     cout << '\r' << pct_done*100 << "%" << flush; */
        /*     pct_done += 0.01; */
        /* } */
    }

    if (args.verbose)
        cout << endl;

    return (double) num_correct / (double) num_total;
}

vector<label_data> bow_encode(NNU::Dictionary* NNU_D, string label_file,
                              cmd_arguments args, double& total_encode_time,
                              MatrixXd& vappt) {
    vector<label> labels = extract_labels(label_file);
    vector<label_data> datum;
    double pct_done = 0.01;
    MatrixXd D = NNU_D->get_D();
    static MatrixXd vapptD = vappt * D;
    MatrixXd x;
    MatrixXd z;
    VectorXd v;

    for (int i = 0; i < labels.size(); i++) {
        EigenIO::read_npy_half((args.data_folder + labels[i].fname).c_str(), x);

        l2_norm(x);

        double start_t = get_wall_time();

        //perform encoding
        if (args.encoder == "nns") {
            z = NNU::nns(NNU_D, vappt, x, D);
        } else if (args.encoder == "par-nns") {
            z = NNU::par_nns(NNU_D, vappt, x, D);
        } else if (args.encoder == "nns-pca-x") {
            z = NNU::nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "par-nns-pca-x") {
            z = NNU::par_nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "nns-pca-d") {
            z = NNU::nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "par-nns-pca-d") {
            z = NNU::par_nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "nnu") {
            z = NNU::nnu(NNU_D, vappt, x, D);
        } else if (args.encoder == "par-nnu") {
            z = NNU::par_nnu(NNU_D, vappt, x, D);
        } else if (args.encoder == "nnu-pca") {
            z = NNU::nnu_pca(NNU_D, vappt, vapptD, x, D);
        } else if (args.encoder == "par-nnu-pca") {
            z = NNU::par_nnu_pca(NNU_D, vappt, vapptD, x, D);
        } else {
            cout << "Invalid encoder for BoW.\n";
            exit(1);
        }

        total_encode_time += get_wall_time() - start_t;
        v = convert_to_bow(z, args.num_atoms);
        datum.push_back(label_data(labels[i].class_id, v));

        if (args.verbose && (float) i / labels.size() > pct_done) {
            cout << '\r' << pct_done*100 << "%, encoding time: " << total_encode_time << flush;
            pct_done += 0.01;
        }
    }

    if (args.verbose)
        cout << endl;

    return datum;
}

vector<label_data> pool_encode(NNU::Dictionary* NNU_D, string label_file,
                              cmd_arguments args, double& total_encode_time,
                              MatrixXd& vappt) {
    vector<label> labels = extract_labels(label_file);
    vector<label_data> datum;
    double pct_done = 0.01;
    MatrixXd D = NNU_D->get_D();
    MatrixXd x;
    MatrixXd z;
    VectorXd v;

    for (int i = 0; i < labels.size(); i++) {
        EigenIO::read_npy_half((args.data_folder + labels[i].fname).c_str(), x);

        l2_norm(x);
        double start_t = get_wall_time();

        //perform encoding
        if (args.encoder == "mp") {
            z = NNU::mp(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-mp") {
            z = NNU::par_mp(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "mp-pca-x") {
            z = NNU::mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-mp-pca-x") {
            z = NNU::par_mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "mp-pca-d") {
            z = NNU::mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-mp-pca-d") {
            z = NNU::par_mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "nnump") {
            z = NNU::nnump(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-nnump") {
            z = NNU::par_nnump(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "nnump-pca") {
            z = NNU::nnump_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-nnump-pca") {
            z = NNU::par_nnump_pca(NNU_D, vappt, x, D, args.k);
        } else {
            cout << "Invalid encoder for MP.\n";
            exit(1);
        }

        total_encode_time += get_wall_time() - start_t;
        v = pool_all(z);
        datum.push_back(label_data(labels[i].class_id, v));

        if (args.verbose && (float) i / labels.size() > pct_done) {
            cout << '\r' << pct_done*100 << "%, encoding time: " << total_encode_time << flush;
            pct_done += 0.01;
        }
    }

    if (args.verbose)
        cout << endl;

    return datum;
}

/* Encodes only the first filename in the label folder */
vector<label_data> mp_encode(NNU::Dictionary* NNU_D, string label_file,
                   cmd_arguments args, double& total_encode_time,
                   MatrixXd& vappt) {
    vector<label> labels = extract_labels(label_file);
    double pct_done = 0.01;
    MatrixXd D = NNU_D->get_D();
    MatrixXd x;
    MatrixXd z;

    EigenIO::read_npy_half((args.data_folder + labels[0].fname).c_str(), x);
    double start_t = get_wall_time();

    //perform encoding
        if (args.encoder == "mp") {
            z = NNU::mp(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-mp") {
            z = NNU::par_mp(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "mp-pca-x") {
            z = NNU::mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-mp-pca-x") {
            z = NNU::par_mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "mp-pca-d") {
            z = NNU::mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-mp-pca-d") {
            z = NNU::par_mp_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "nnump") {
            z = NNU::nnump(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-nnump") {
            z = NNU::par_nnump(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "nnump-pca") {
            z = NNU::nnump_pca(NNU_D, vappt, x, D, args.k);
        } else if (args.encoder == "par-nnump-pca") {
            z = NNU::par_nnump_pca(NNU_D, vappt, x, D, args.k);
        } else {
            cout << "Invalid encoder for MP.\n";
            exit(1);
        }

    total_encode_time += get_wall_time() - start_t;

    //Stuff our single example in vector<label_data> to match other encoders
    label_data lena(0, z);
    vector<label_data> lena_data {lena};
    return lena_data;
}

void threshold_small(VectorXd &X) {
    for (int i = 0; i < X.size(); ++i) {
        if (X(i) < 1e-9) {
            X(i) = 0;
        }
    }
}

void threshold_small(MatrixXd &X) {
    for (int i = 0; i < X.rows(); ++i) {
        for (int j = 0; j < X.cols(); ++j) {
            if (X(i, j) < 1e-9) {
                X(i, j) = 0;
            }
        }
    }
}

#endif
