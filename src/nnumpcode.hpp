#ifndef NNUMPCODE_HPP_
#define NNUMPCODE_HPP_

#include "nnudictionary.hpp"

#include <algorithm>
#include <thread>
#include <Eigen/Dense>
#include <Eigen/Sparse>

using namespace std;
using namespace Eigen;

typedef SparseVector<double> SparseXd;
typedef SparseMatrix<double> SparseMatrixXd;

namespace NNU {

double computeMSE(int cols, MatrixXd &X, MatrixXd &D, MatrixXd &zNNU) {
	double meanmse = 0;
	for (int i = 0; i < cols; ++i) {
		VectorXd cX = X.col(i);
		VectorXd V = cX - D * zNNU.col(i);
		meanmse += V.cwiseAbs2().sum() / V.rows();
	}
	meanmse /= cols;
	return meanmse;
}

VectorXd computeVtX(MatrixXd &Vappt, VectorXd &cX) {
	return Vappt * cX;
}

vector<size_t> candidatesLookup(VectorXd &vtx, Dictionary *vtd) {

	/* return vtd->parTableAtomLookup(vtx); */
	return vtd->atomLookup(vtx);

}

int computeMaxDot(const VectorXd &cX, MatrixXd &D) {
	double maxcoeff = 0.0;
	double tmpcoeff = 0.0;
	int maxidx = 0.0;
	int cols = D.cols();
	for (int idx = 0; idx < cols; ++idx) {
		tmpcoeff = std::abs(cX.dot(D.col(idx)));
		if (tmpcoeff > maxcoeff) {
			maxcoeff = tmpcoeff;
			maxidx = idx;
		}
	}
	return maxidx;
}

int computeCtX(vector<size_t> &atomIdxs, VectorXd &cX, MatrixXd &D) {
	double maxcoeff = 0.0;
	double tmpcoeff = 0.0;
	int maxidx = 0.0;
	for (auto idx : atomIdxs) {
		tmpcoeff = std::abs(cX.dot(D.col(idx)));
		if (tmpcoeff > maxcoeff) {
			maxcoeff = tmpcoeff;
			maxidx = idx;
		}
	}

	return maxidx;
}

MatrixXd nns(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D) {
	int cols = X.cols();
	VectorXd ret(cols);

	for (int i = 0; i < cols; ++i) {
		ret[i] = computeMaxDot(X.col(i), D);
	}

	return ret;
}

MatrixXd par_nns(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D) {
	int cols = X.cols();
	VectorXd ret(cols);

#pragma omp parallel for
	for (int i = 0; i < cols; ++i) {
		ret[i] = computeMaxDot(X.col(i), D);
	}

	return ret;
}

MatrixXd nns_pca(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& vapptD,
		MatrixXd& X, MatrixXd& D) {
	int cols = X.cols();
	/* VectorXd vtx; */
	VectorXd ret(cols);

	for (int i = 0; i < cols; ++i) {
		/* vtx = Vappt * X.col(i); */
		ret[i] = computeMaxDot(Vappt * X.col(i), vapptD);
	}

	return ret;
}

MatrixXd par_nns_pca(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& vapptD,
		MatrixXd& X, MatrixXd& D) {
	int cols = X.cols();
	/* VectorXd vtx; */
	VectorXd ret(cols);

#pragma omp parallel for
	for (int i = 0; i < cols; ++i) {
		/* vtx = Vappt * X.col(i); */
		ret[i] = computeMaxDot(Vappt * X.col(i), vapptD);
	}

	return ret;
}

VectorXd nnu(Dictionary *vtd, MatrixXd &Vappt, MatrixXd &X, MatrixXd &D) {
	int maxidx = 0;
	int cols = X.cols();
	double maxcoeff = 0.0;
	double tmpcoeff;
	vector<size_t> atomIdxs;
	VectorXd vtx;
	VectorXd ret(cols);

	for (int i = 0; i < cols; ++i) {
		vtx = Vappt * X.col(i);
		atomIdxs = candidatesLookup(vtx, vtd);
		for (auto idx : atomIdxs) {
			tmpcoeff = std::abs(X.col(i).dot(D.col(idx)));
			if (tmpcoeff > maxcoeff) {
				maxcoeff = tmpcoeff;
				maxidx = idx;
			}
		}
		ret(i) = maxidx;
		maxcoeff = 0.0;
	}

	return ret;
}

VectorXd par_nnu(Dictionary *vtd, MatrixXd &Vappt, MatrixXd &X, MatrixXd &D) {
	MatrixXd vapptX = Vappt * X;
	unsigned concurentThreadsSupported = thread::hardware_concurrency();
	unsigned rows = vtd->rows;
	return vtd->batchParTableAtomLookup(X, D, vapptX,
			std::min(concurentThreadsSupported, rows), vtd->cols);
}

VectorXd nnu_pca(Dictionary *vtd, MatrixXd &Vappt, MatrixXd &vapptD,
		MatrixXd &X, MatrixXd &D) {
	int maxidx = 0;
	int cols = X.cols();
	double maxcoeff = 0.0;
	double tmpcoeff = 0.0;
	vector<size_t> atomIdxs;
	VectorXd vtx;
	VectorXd ret(cols);
	/* static MatrixXd vapptD = Vappt * D; */

	for (int i = 0; i < cols; ++i) {
		vtx = Vappt * X.col(i);
		atomIdxs = candidatesLookup(vtx, vtd);
		for (auto idx : atomIdxs) {
			tmpcoeff = std::abs(vtx.dot(vapptD.col(idx)));

			if (tmpcoeff >= maxcoeff) {
				maxcoeff = tmpcoeff;
				maxidx = idx;
			}
		}
		ret(i) = maxidx;
		maxcoeff = 0.0;
		tmpcoeff = 0.0;
	}

	return ret;
}

VectorXd par_nnu_pca(Dictionary *vtd, MatrixXd &Vappt, MatrixXd &vapptD,
		MatrixXd &X, MatrixXd &D) {
	MatrixXd vapptX = Vappt * X;
	/* MatrixXd vapptD = Vappt * D; */
	unsigned concurentThreadsSupported = thread::hardware_concurrency();
	unsigned rows = vtd->rows;
	return vtd->batchParTableAtomLookup(vapptX, vapptD, vapptX,
			std::min(concurentThreadsSupported, rows), vtd->cols);
}

MatrixXd mp(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D, int k) {
	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());

	int max;
	int cols = X.cols();
	int actualIdx;
	double dotCoeff;
	VectorXd cX;
	VectorXd atom;
	int tmp;

	for (int i = 0; i < cols; ++i) {
		cX = X.col(i);

		for (int j = 0; j < k; ++j) {
			actualIdx = computeMaxDot(cX, D);

			atom = D.col(actualIdx);
			dotCoeff = cX.dot(atom);
			tmp = z(i, actualIdx);
			z(i, actualIdx) += dotCoeff;

			/* if (z(i, actualIdx) > 1e10) { */
			/*     cout << z(i, actualIdx) << endl; */
			/*     cout << tmp << endl; */
			/*     cout << dotCoeff << endl; */
			/*     /1* cout << cX << endl; *1/ */
			/*     /1* cout << atom << endl; *1/ */
			/*     assert(false); */
			/* } */

			cX -= dotCoeff * atom;
		}
	}

	return z;
}

MatrixXd par_mp(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D,
		int k) {
	Eigen::initParallel();

	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());
	int cols = X.cols();

#pragma omp parallel for
	for (int i = 0; i < cols; ++i) {
		VectorXd atom;
		VectorXd cX = X.col(i);
		double dotCoeff;
		int actualIdx;

		for (int j = 0; j < k; ++j) {
			actualIdx = computeMaxDot(cX, D);

			atom = D.col(actualIdx);
			dotCoeff = cX.dot(atom);
			z(i, actualIdx) += dotCoeff;

			cX -= dotCoeff * atom;
		}
	}

	return z;
}

MatrixXd nnump(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D,
		int k) {
	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());
	int max;
	int cols = X.cols();
	int actualIdx;
	double dotCoeff;
	vector<size_t> atomIdxs;
	VectorXd vtx;
	VectorXd cX;
	VectorXd atom;

	for (int i = 0; i < cols; ++i) {
		cX = X.col(i);

		for (int j = 0; j < k; ++j) {
			vtx = Vappt * cX;
			atomIdxs = candidatesLookup(vtx, vtd);
			actualIdx = computeCtX(atomIdxs, cX, D);

			//Compute residual
			atom = D.col(actualIdx);
			dotCoeff = cX.dot(atom);
			z(i, actualIdx) += dotCoeff;
			cX -= dotCoeff * atom;
		}
	}

	return z;
}

MatrixXd par_nnump(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D,
		int k) {
	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());
	MatrixXd vtx;
	MatrixXd cX = X;

	unsigned concurentThreadsSupported = thread::hardware_concurrency();
	unsigned rows = vtd->rows;

	for (int j = 0; j < k; ++j) {
		vtx = Vappt * cX;

		VectorXd actualIdxs = vtd->batchParTableAtomLookup(cX, D, vtx,
				std::min(concurentThreadsSupported, rows), vtd->cols);

		int sz = actualIdxs.size();
		//Compute residual
#pragma omp parallel for
		for (int l = 0; l < sz; ++l) {
			int actualIdx = actualIdxs[l];
			VectorXd atom = D.col(actualIdx);
			double dotCoeff = cX.col(l).dot(atom);
			z(l, actualIdx) += dotCoeff;
			cX.col(l) -= dotCoeff * atom;
		}
	}

	return z;
}

MatrixXd nnump_pca(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D,
		int k) {
	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());
	int max;
	int cols = X.cols();
	int actualIdx;
	double dotCoeff;
	vector<size_t> atomIdxs;
	VectorXd vtx;
	VectorXd cX;
	VectorXd atom;
	static MatrixXd vapptD = Vappt * D;

	for (int i = 0; i < cols; ++i) {
		cX = X.col(i);

		for (int j = 0; j < k; ++j) {
			vtx = Vappt * cX;
			atomIdxs = candidatesLookup(vtx, vtd);
			actualIdx = computeCtX(atomIdxs, vtx, vapptD);

			atom = D.col(actualIdx);
			dotCoeff = cX.dot(atom);
			z(i, actualIdx) += dotCoeff;

			cX -= dotCoeff * atom;
		}
	}

	return z;
}

MatrixXd par_nnump_pca(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X,
		MatrixXd& D, int k) {
	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());
	MatrixXd vtx;
	MatrixXd cX = X;

	static MatrixXd vapptD = Vappt * D;

	unsigned concurentThreadsSupported = thread::hardware_concurrency();
	unsigned rows = vtd->rows;

	for (int j = 0; j < k; ++j) {
		vtx = Vappt * cX;

		VectorXd actualIdxs = vtd->batchParTableAtomLookup(vtx, vapptD, vtx,
				std::min(concurentThreadsSupported, rows), vtd->cols);

		int sz = actualIdxs.size();
		//Compute residual
#pragma omp parallel for
		for (int l = 0; l < sz; ++l) {
			int actualIdx = actualIdxs[l];
			VectorXd atom = D.col(actualIdx);
			double dotCoeff = cX.col(l).dot(atom);
			z(l, actualIdx) += dotCoeff;
			cX.col(l) -= dotCoeff * atom;
		}
	}

	return z;
}

MatrixXd mp_pca(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D,
		int k) {
	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());

	int max;
	int cols = X.cols();
	int actualIdx;
	double dotCoeff;
	VectorXd cX;
	VectorXd vappycX;
	VectorXd atom;
	int tmp;
	static MatrixXd vapptD = Vappt * D;

	for (int i = 0; i < cols; ++i) {
		cX = X.col(i);
		vappycX = Vappt * cX;
		for (int j = 0; j < k; ++j) {
			actualIdx = computeMaxDot(vappycX, vapptD);

			atom = D.col(actualIdx);
			dotCoeff = cX.dot(atom);
			tmp = z(i, actualIdx);
			z(i, actualIdx) += dotCoeff;
			cX -= dotCoeff * atom;
		}
	}

	return z;
}

MatrixXd par_mp_pca(Dictionary* vtd, MatrixXd& Vappt, MatrixXd& X, MatrixXd& D,
		int k) {
	Eigen::initParallel();

	MatrixXd z = MatrixXd::Zero(X.cols(), D.cols());

	int cols = X.cols();
	static MatrixXd vapptD = Vappt * D;

#pragma omp parallel for
	for (int i = 0; i < cols; ++i) {
		int actualIdx;
		double dotCoeff;
		VectorXd atom;
		int tmp;
		VectorXd cX = X.col(i);
		VectorXd vappycX = Vappt * cX;
		for (int j = 0; j < k; ++j) {
			actualIdx = computeMaxDot(vappycX, vapptD);

			atom = D.col(actualIdx);
			dotCoeff = cX.dot(atom);
			tmp = z(i, actualIdx);
			z(i, actualIdx) += dotCoeff;
			cX -= dotCoeff * atom;
		}
	}

	return z;
}

}

#endif
