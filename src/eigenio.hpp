#ifndef EIGENIO_HPP_
#define EIGENIO_HPP_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "cnpy.h"
#include "half.hpp"
using namespace std;
using namespace Eigen;
using half_float::half;

namespace EigenIO {
const static IOFormat CSVFormat(StreamPrecision, DontAlignCols, ",", "\n");

/* void writeMatToCSVfile(string name, MatrixXd matrix) */
/* { */
/*   ofstream file(name.c_str()); */

/*   for(int  i = 0; i < matrix.rows(); i++){ */
/*       for(int j = 0; j < matrix.cols(); j++){ */
/*          string str = std::to_string(matrix(i,j)); */
/*          if(j+1 == matrix.cols()){ */
/*              file<<str; */
/*          }else{ */
/*              file<<str<<','; */
/*          } */
/*       } */
/*       file<<'\n'; */
/*   } */
/* } */

void writeToCSVfile(string name, MatrixXd mat) {
    ofstream file(name.c_str());
    file << mat.format(CSVFormat);
}

void writeToCSVfile(string name, VectorXd vec) {
    ofstream file(name.c_str());
    file << vec.format(CSVFormat);
}


Eigen::MatrixXd readCSV(std::string file) {
    std::ifstream data(file);
    std::string line;
    int rows = 0;
    int cols = 0;

    while(std::getline(data, line)) {
        std::stringstream lineStream(line);
        std::string cell;
        cols = 0;
        while(std::getline(lineStream, cell, ',')) {
            cols++;
        }
        rows++;
    }

    MatrixXd res(rows, cols);

    //rewind file to beginning
    data.clear();
    data.seekg(0, data.beg);
    int i = 0;
    int j = 0;
    while(std::getline(data, line)) {
        std::stringstream lineStream(line);
        std::string cell;
        j = 0;
        while(std::getline(lineStream, cell, ',')) {
            res(i, j) = std::stod(cell);
            j++;
        }
        i++;
    }

  return res;
}

template<class Matrix>
void write_binary(const char* filename, const Matrix& matrix) {
    std::ofstream out(filename, ios::out | ios::binary | ios::trunc);
    typename Matrix::Index rows = matrix.rows(), cols = matrix.cols();
    out.write((char*) (&rows), sizeof(typename Matrix::Index));
    out.write((char*) (&cols), sizeof(typename Matrix::Index));
    out.write((char*) matrix.data(),
            rows * cols * sizeof(typename Matrix::Scalar));
    out.close();
}

template<class Matrix>
void read_binary(const char* filename, Matrix& matrix) {
    std::ifstream in(filename, ios::in | std::ios::binary);
    typename Matrix::Index rows = 0, cols = 0;
    in.read((char*) (&rows), sizeof(typename Matrix::Index));
    in.read((char*) (&cols), sizeof(typename Matrix::Index));
    matrix.resize(rows, cols);
    in.read((char *) matrix.data(),
            rows * cols * sizeof(typename Matrix::Scalar));
    in.close();
}

template<class Matrix>
void read_npy_half(const char* filename, Matrix& matrix) {
    string fname(filename);
    cnpy::NpyArray arr = cnpy::npy_load(fname);
    half* mv1 = reinterpret_cast<half*>(arr.data);
    int rows = arr.shape[1];
    int cols = arr.shape[0];

    matrix.resize(rows, cols);

    for (int i = 0; i < cols; i++)
        for (int j = 0; j < rows; j++)
            matrix(j, i) = mv1[j + i * rows];

    delete[] mv1;

}

template<class Matrix>
void read_npy_double(const char* filename, Matrix& matrix) {
    string fname(filename);
    cnpy::NpyArray arr = cnpy::npy_load(fname);
    double* mv1 = reinterpret_cast<double*>(arr.data);
    int rows = arr.shape[1];
    int cols = arr.shape[0];

    matrix.resize(rows, cols);

    for (int i = 0; i < cols; i++)
        for (int j = 0; j < rows; j++)
            matrix(j, i) = mv1[j + i * rows];

    delete[] mv1;
}

template<class Matrix>
void write_npy(const char* filename, const Matrix& matrix) {
    string fname(filename);
    Eigen::MatrixXd temp = matrix.transpose();

    unsigned int shape[2] = { };
    shape[0] = matrix.rows();
    shape[1] = matrix.cols();
    double* data = new double[matrix.rows() * matrix.cols()];
    for (int i = 0; i < matrix.rows(); i++)
        for (int j = 0; j < matrix.cols(); j++)
            data[j + i * matrix.cols()] = matrix(i, j);

    cnpy::npy_save(fname, data, shape, 2, "w");
}

}

#endif
