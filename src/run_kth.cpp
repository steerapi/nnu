#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <glob.h>
#include <sys/mman.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <sys/stat.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "nnudictionary.hpp"
#include "nnumpcode.hpp"
#include "tictoc.hpp"
#include "eigenio.hpp"
#include "utilities.cpp"
#include "batch_encode.cpp"
#include "pca.hpp"

using namespace std;

void run_test(cmd_arguments args) {
    double total_encode_time = 0.0;
    int x_dim = get_Xdim(args.data_folder, args.train_file);
    int num_multadd;
    ostringstream oss;
    string cmd_str;
    string km_data = "KM_tr_data.npy";
    MatrixXd D;
    NNU::Dictionary* NNU_D;
    PCA pca(args.alpha);

    if (args.encoder == "pca-on-x") {
        oss << "_pca" << args.alpha;
        args.KM_path.append(oss.str());
        oss.str("");
        oss << "KM_tr_data_dim" << args.alpha << ".npy";
        km_data = oss.str();
        oss.str("");
    }

    if (file_exists(args.output_path + args.KM_path + ".npy") && args.encoder != "pca-on-x") {
        if (args.verbose)
            cout << "Loading learned centroids from cache\n";
    } else {
        MatrixXd train_data = extract_data(args.data_folder, args.train_file,
                                           args.KM_tr_data, args.verbose);

        //Must reduce dim for pca-on-x
        if (args.encoder == "pca-on-x") {
            if (args.verbose)
                cout << "Fitting PCA for X" << endl;

            train_data = pca.fit_transform(train_data.block(0, 0, 2000, train_data.cols()));
        }

        string save_path = args.output_path + km_data;
        EigenIO::write_npy(save_path.c_str(), train_data);

        oss << "python " + args.python_path + "trainKM.py --num_clusters=" <<
               args.KM_clusters << " --data=" << save_path << " --output=" <<
               args.output_path + args.KM_path;

        cmd_str = oss.str();
        oss.str("");
        system(cmd_str.c_str());
    }

    string load_path = args.output_path + args.KM_path + ".npy";
    EigenIO::read_npy_double(load_path.c_str(), D);

    l2_norm(D);

    JacobiSVD<MatrixXd> svd(D.transpose(), ComputeFullV);
    MatrixXd V = svd.matrixV();

    MatrixXd vapp = V.block(0, 0, V.rows(), args.alpha);
    MatrixXd vappt = vapp.transpose();

    if (file_exists(args.output_path + args.NNU_path)) {
        if (args.verbose)
            cout << "Loaded cached NNU Dictionary\n";

        NNU_D = new NNU::Dictionary(args.alpha, D.cols(), args.beta, D, vappt);
        NNU_D->load((args.output_path + args.NNU_path).c_str());

    } else {
        if (args.verbose)
            cout << "Converting NNU Dictionary\n";

        NNU_D = NNU::train_dict(D, args.alpha, args.beta, args.verbose);

        //Don't save for pca-on-x
        if (args.encoder != "pca-on-x")
            NNU_D->save((args.output_path + args.NNU_path).c_str());
    }


    if (args.verbose)
        cout << "Encoding training files\n";

    vector<label_data> tr_data = bow_encode(NNU_D, args.train_file, args, total_encode_time, pca);

    if (args.verbose)
        cout << "Encoding testing files\n";

    vector<label_data> t_data = bow_encode(NNU_D, args.test_file, args, total_encode_time, pca);

    //save labels used for classification accuracy
    save_label_data(tr_data, args.labels_tr_path, args.data_tr_path);
    save_label_data(t_data, args.labels_t_path, args.data_t_path);

    //build python string to do prediction
    oss << "python " + args.python_path + "predict.py --tr_data=" <<
        args.data_tr_path << " --tr_label=" << args.labels_tr_path <<
        " --t_data=" << args.data_t_path << " --t_label=" << args.labels_t_path <<
        " --kernel=" << args.svm_kernel;

    if (args.verbose)
        oss << " --verbose ";

    cmd_str = oss.str();
    oss.str("");

    //compute #mult+add
    if (args.encoder.find("nnu") != string::npos) { //nnu case
        num_multadd = comp_num_multadd_nnu(args.alpha, args.beta, x_dim);
    } else { //nns case
        num_multadd = comp_num_multadd_nns(args.KM_clusters, x_dim);
    }

    //output results
    cout << args.encoder << ", "
         << num_multadd << ", "
         << args.KM_clusters << ", "
         << args.alpha << ", " 
         << args.beta << ", " 
         << flush;

    //execute prediction script (will output results
    system(cmd_str.c_str());

    if (args.verbose)
        cout << "Total encoding time: ";

    cout << total_encode_time << flush << endl;
}


int main(int argc, char *argv[]) {
    /* Parse commandline args */
    cmd_arguments args(argc, argv);

    /* Turn off multi-threading for Eigen */
    Eigen::setNbThreads(1);

    run_test(args);

    return 0;
}
