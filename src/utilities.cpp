#ifndef UTILITIES_CPP_
#define UTILITIES_CPP_

#include <iostream>
#include <algorithm>
#include <fstream>
#include <cmath>
#include <vector>
#include <set>
#include <mutex>
#include <thread>
#include <sstream>
#include <sys/time.h>
#include <sys/stat.h>

#include "../hashlist/src/hashtable.hpp"
#include "half.hpp"
#include "sortindex.hpp"
#include <Eigen/Dense>
#include <boost/python.hpp>
#include <boost/numpy.hpp>
#include <boost/algorithm/string/join.hpp>
#include "nnudictionary.hpp"
#include "nnumpcode.hpp"
#include "eigenio.hpp"
#include "structs.h"

using namespace Eigen;

/* command line args struct for scripts */
struct cmd_arguments {
    int alpha, beta, k, num_atoms, num_tr_data, max_pca_tr, num_multadd;
    string output_path, data_folder, train_file, test_file, encoder, dictionary_path,
           NNU_path, labels_tr_path, labels_t_path, data_tr_path, data_t_path,
           python_path, svm_kernel, application, cluster_method, raw_data_path;
    std::vector<label_data> (*pEncode)(NNU::Dictionary*, string, cmd_arguments,
                                       double&, MatrixXd&); 
    double (*pcEncode)(NNU::Dictionary*, string, cmd_arguments, MatrixXd&); 

    bool verbose;
    std::vector<string> encoders = {"nns", "par-nns", "nns-pca-x", "par-nns-pca-x", 
                                    "nns-pca-d", "par-nns-pca-d", "mp", "par-mp", 
                                    "mp-pca-x", "par-mp-pca-x", "mp-pca-d", "par-mp-pca-d",
                                    "nnu", "par-nnu", "nnu-pca", "par-nnu-pca",
                                    "nnump", "par-nnump", "nnump-pca", "par-nnump-pca"};
    std::vector<string> svm_kernels = {"linear", "chi2"};
    std::vector<string> applications = {"kth", "cifar", "lena"};
    std::vector<string> cluster_methods = {"KM", "KSVD"};

    void printhelp() {
        cout << "\n-p application (" + boost::algorithm::join(applications, ", ") + ")\n" + 
                "-e encoder (" + boost::algorithm::join(encoders, ", ") + ")\n" + 
                "-s kernel for SVM (" + boost::algorithm::join(svm_kernels, ", ") + ")\n" + 
                "-d data folder path containing .npz files\n"
                "-o output folder path to store intermediate results\n"
                "-r train file path\n"
                "-t test file path\n"
                "-c clustering method for Dictionary (" + boost::algorithm::join(cluster_methods, ", ")  + ")\n" +
                "-y path to python folder\n"
                "-w # of dictionary atoms\n"
                "-q # of samples used to train dictionary\n"
                "-a alpha parameter for nnu variants\n"
                "-b beta parameter for nnu variants\n"
                "-k number of iteration in mp, nnu-mp, and pca-nnu-mp\n"
                "-v verbose flag\n"
                "-h help\n";
    }

    bool is_valid_encoder(string encoder) {
        if (std::find(encoders.begin(), encoders.end(), encoder) != encoders.end()) {
            return true;
        }

        cout << encoder << " is an invalid encoder.\n";
        exit(1);
    }

    bool is_valid_application(string application) {
        if (std::find(applications.begin(), applications.end(), application) != applications.end()) {
            return true;
        }

        cout << application << " is an invalid application.\n";
        exit(1);
    }

    bool is_valid_cluster_method(string cluster_method) {
        if (std::find(cluster_methods.begin(), cluster_methods.end(), cluster_method) != cluster_methods.end()) {
            return true;
        }

        cout << cluster_method << " is an invalid cluster method.\n";
        exit(1);
    }


    bool is_valid_kernel(string svm_kernel) {
        if (std::find(svm_kernels.begin(), svm_kernels.end(), svm_kernel) != svm_kernels.end()) {
            return true;
        }

        cout << svm_kernel << " is an invalid SVM kernel.\n";
        exit(1);
    }

    bool is_dir(string pathname) {
        struct stat sb;

        if (stat(pathname.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))
        {
            return true;
        }

        cout << "Folder does not exist: " << pathname << '\n';
        exit(1);

    }

    bool is_file (const std::string& pathname) {
        struct stat buffer;
        if (stat (pathname.c_str(), &buffer) == 0)
            return true;

        cout << "File does not exist: " << pathname << '\n';
        exit(1);
    }

    cmd_arguments(int argc, char** argv) {
        output_path = "output/";
        python_path = "python/";
        svm_kernel = "chi2";
        verbose = false;
        alpha = 1;
        beta = 1;
        k = 1;
        num_atoms = 600;
        num_tr_data = 100000;
        max_pca_tr = 2000;
        /* pEncode = bow_encode; */

        string arg_str = "hp:e:s:d:o:r:t:c:y:w:q:a:b:k:v";
        int option = -1;

        while ((option = getopt(argc, argv, arg_str.c_str())) != -1) {
            switch (option) {
            case 'p':
                application = string(strdup(optarg));
                is_valid_application(application);
                break;
            case 'e':
                encoder = string(strdup(optarg));
                is_valid_encoder(encoder);
                break;
            case 's':
                svm_kernel = string(strdup(optarg));
                is_valid_kernel(svm_kernel);
                break;
            case 'd':
                data_folder = string(strdup(optarg));
                is_dir(data_folder);
                break;
            case 'o':
                output_path = string(strdup(optarg));
                is_dir(output_path);
                break;
            case 'r':
                train_file = string(strdup(optarg));
                is_file(train_file);
                break;
            case 't':
                test_file = string(strdup(optarg));
                is_file(test_file);
                break;
            case 'c':
                cluster_method = string(strdup(optarg));
                is_valid_cluster_method(cluster_method);
                break;
            case 'y':
                python_path = string(strdup(optarg));
                is_dir(python_path);
                break;
            case 'w':
                num_atoms = atoi(optarg);
                break;
            case 'q':
                num_tr_data = atoi(optarg);
                break;
            case 'a':
                alpha = atoi(optarg);
                break;
            case 'b':
                beta = atoi(optarg);
                break;
            case 'k':
                k = atoi(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
                printhelp();
                exit(0);
                break;
            default:
                printhelp();
                exit(1);
                break;
            }
        }

        ostringstream oss;
        //Building dictionary path
        oss << "D"
            << "_app_" << application
            << "_clustermethod_" << cluster_method 
            << "_numatoms_" << num_atoms
            << "_numtrdata_" << num_tr_data;

        dictionary_path = output_path + oss.str();
        oss.str("");

        //Building cached raw data paths
        oss << "raw"
            << "_app_" << application
            << "_numtrdata_" << num_tr_data;

        raw_data_path = output_path + oss.str();
        oss.str("");

        //Building NNU path
        oss << "NNU_D" 
            << "_app_" << application
            << "_clustermethod_" << cluster_method 
            << "_numatoms_" << num_atoms
            << "_numtrdata_" << num_tr_data
            << "_alpha_" << alpha 
            << "_beta_" << beta;

        if (encoder == "pca-on-x") {
            oss << "_pca-on-x";
        }
       
        oss << ".iram";

        NNU_path = output_path + oss.str();
        oss.str("");

        //Building encoded data/label paths
        oss << "alpha_" << alpha 
            << "_beta_" << beta 
            << "_k_" << k 
            << "_numatoms_" << num_atoms << ".txt";
        string modifiers = oss.str();
        oss.str("");

        labels_tr_path = output_path + "labels_tr_" + modifiers;
        labels_t_path = output_path + "labels_t_" + modifiers;
        data_tr_path = output_path + "data_tr_" + modifiers;
        data_t_path = output_path + "data_t_" + modifiers;

        int x_dim;
        if (encoder.find("pca") == string::npos) {
            x_dim = get_Xdim(data_folder, train_file); //no reduction, so just X dim
        } else {
            x_dim = alpha;  //reduction, so reduction factor (alpha)
        }

        //compute #mult+add
        if (encoder.find("nnu") != string::npos) { //nnu case
            num_multadd = comp_num_multadd_nnu(alpha, beta, x_dim);
        } else { //nns case
            num_multadd = comp_num_multadd_nns(num_atoms, x_dim);
        }

    }

};

#endif
