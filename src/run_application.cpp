#ifndef RUN_APPLICATION_CPP_
#define RUN_APPLICATION_CPP_

#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <glob.h>
#include <sys/mman.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <sys/stat.h>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "nnudictionary.hpp"
#include "tictoc.hpp"
#include "utilities.cpp"
#include "eigenio.hpp"
#include "batch_encode.cpp"

using namespace std;

//Calls the python script to train a dictionary
MatrixXd python_run_dictionary(cmd_arguments args, string tmp_tr_path) {
    ostringstream oss;
    MatrixXd D;
    oss << "python " << args.python_path << "train_dictionary.py"
        << " --num_clusters=" << args.num_atoms
        << " --method=" << args.cluster_method
        << " --data=" << tmp_tr_path
        << " --k=" << args.k
        << " --output=" << args.dictionary_path;

    string cmd_str = oss.str();
    system(cmd_str.c_str());

    EigenIO::read_npy_double((args.dictionary_path + ".npy").c_str(), D);

    return D;
}

void python_run_prediction(cmd_arguments args) {
    //build python string to do prediction
    string cmd_str;
    ostringstream oss;
    oss << "python " + args.python_path + "predict.py"
        << " --tr_data=" << args.data_tr_path 
        << " --tr_label=" << args.labels_tr_path 
        << " --t_data=" << args.data_t_path 
        << " --t_label=" << args.labels_t_path 
        << " --kernel=" << args.svm_kernel;

    if (args.verbose)
        oss << " --verbose ";

    cmd_str = oss.str();
    oss.str("");

    //execute prediction script (will output results)
    system(cmd_str.c_str());
    cout << flush;
}

void python_run_psnr(cmd_arguments args, MatrixXd enc_patches) {
    //save patches for psnr
    string lena_outpath = args.output_path + "lena_patches.npy";
    EigenIO::write_npy(lena_outpath.c_str(), enc_patches);

    //build python string to do prediction
    string cmd_str;
    ostringstream oss;
    oss << "python " + args.python_path + "psnr.py"
        << " --encoded=" << lena_outpath
        << " --dictionary=" << args.dictionary_path + ".npy";

    if (args.verbose)
        oss << " --verbose ";

    cmd_str = oss.str();
    oss.str("");

    //execute prediction script (will output results)
    system(cmd_str.c_str());
    cout << flush;
}

//Trains a dictionary and caches it for further use
MatrixXd train_dictionary(cmd_arguments args, MatrixXd &D) {
    MatrixXd train_data;

    //Check for cached training data
    if(!file_exists(args.raw_data_path)) {
        train_data = extract_data(args.data_folder, args.train_file, args.num_tr_data,
                                  args.verbose);
        EigenIO::write_npy(args.raw_data_path.c_str(), train_data.transpose());
    } else {
        EigenIO::read_npy_double(args.raw_data_path.c_str(), train_data);
    }

    
    if (args.verbose)
        cout << "Learning Dictionary" << endl;

    //Train Dictionary
    string tmp_tr_path = args.output_path + "tr.npy";
    EigenIO::write_npy(tmp_tr_path.c_str(), train_data);
    D = python_run_dictionary(args, tmp_tr_path);

    return D;
}


void run_batch_encoder(cmd_arguments args, NNU::Dictionary* NNU_D, MatrixXd &vappt) {
    double ser_encode_time = 0.0;
    double par_encode_time = 0.0;
    vector<label_data> tr_data;
    vector<label_data> t_data;

    //Serial encoding
    if (args.verbose)
        cout << "Encoding training files (serial)\n";

    tr_data = args.pEncode(NNU_D, args.train_file, args, ser_encode_time, vappt);

    if (args.application == "kth" || args.application == "cifar") {
        if (args.verbose)
            cout << "Encoding testing files (serial)\n";

        t_data = args.pEncode(NNU_D, args.test_file, args, ser_encode_time, vappt); 
    }

    //Parallel encoding (only for the timing information)
    string ser_encoder = args.encoder;
    args.encoder = "par-" + args.encoder;  //add par in front of encoder name
    if (args.verbose)
        cout << "Encoding training files (parallel)\n";

    args.pEncode(NNU_D, args.train_file, args, par_encode_time, vappt);

    if (args.application == "kth" || args.application == "cifar") {
        if (args.verbose)
            cout << "Encoding testing files (parallel)\n";

        args.pEncode(NNU_D, args.test_file, args, par_encode_time, vappt);
    }

    //output results
    cout << ser_encoder << ", "
         << args.num_multadd << ", "
         << args.num_atoms << ", "
         << args.alpha << ", "
         << args.beta << ", "
         << ser_encode_time << ", "
         << par_encode_time << ", "
         << flush;

    if (args.application == "kth" || args.application == "cifar") {
        //save labels used for classification accuracy
        save_label_data(tr_data, args.labels_tr_path, args.data_tr_path);
        save_label_data(t_data, args.labels_t_path, args.data_t_path);

        //do prediction in python
        python_run_prediction(args);
    } else if (args.application == "lena") {
        python_run_psnr(args, tr_data[0].mat_data);
    }

    cout << endl;
}

//select the batch encoder to use (related to application)
void select_batch_encoder(cmd_arguments &args) {
    if (args.application == "kth") {
        args.pEncode = bow_encode;
    } else if (args.application == "cifar") {
        args.pEncode = pool_encode;
    } else if (args.application == "lena") {
        args.pEncode = mp_encode;
    }
}

void run_application(cmd_arguments args) {
    MatrixXd D;
    MatrixXd vappt;
    NNU::Dictionary* NNU_D;
    select_batch_encoder(args); //sets function pointer for batch encode

    //Check for cached dictionary
    if (!file_exists(args.dictionary_path + ".npy")) {
        D = train_dictionary(args, D);
    } else {
        if (args.verbose)
            cout << "Using cached Dictionary" << endl;

        EigenIO::read_npy_double((args.dictionary_path + ".npy").c_str(), D);
    }

    l2_norm(D);

    //Get V using X
    if (args.encoder == "nns-pca-x" || args.encoder == "mp-pca-x") {
        MatrixXd train_data = extract_data(args.data_folder, args.train_file,
                                           args.num_tr_data, false);
        int rows = min(int(train_data.rows()), args.max_pca_tr);

        //Create items for NNU Dictionary
        JacobiSVD<MatrixXd> svd(train_data.block(0, 0, rows, train_data.cols()), ComputeThinV);
        MatrixXd V = svd.matrixV();
        vappt = V.block(0, 0, V.rows(), args.alpha).transpose();

    } else { //Get V using D
        //Create items for NNU Dictionary
        JacobiSVD<MatrixXd> svd(D.transpose(), ComputeThinV);
        MatrixXd V = svd.matrixV();
        vappt = V.block(0, 0, V.rows(), args.alpha).transpose();
    }


    //Check for cached NNU dictionary
    if (!file_exists(args.NNU_path)) {
        if (args.verbose)
            cout << "Creating NNU Dictionary\n";

        NNU_D = NNU::train_dict(D, args.alpha, args.beta, args.verbose);
        NNU_D->save(args.NNU_path.c_str());
    } else {
        if (args.verbose)
                cout << "Loaded cached NNU Dictionary\n";

        NNU_D = new NNU::Dictionary(args.alpha, D.cols(), args.beta, D, vappt);
        NNU_D->load(args.NNU_path.c_str());
    }

    run_batch_encoder(args, NNU_D, vappt);
}

int main(int argc, char *argv[]) {
    /* Parse commandline args */
    cmd_arguments args(argc, argv);

    /* Turn off multi-threading for Eigen */
    Eigen::setNbThreads(1);

    /* Fix number of cores for parallelism */
    omp_set_num_threads(10);

    run_application(args);

    return 0;
}

#endif
