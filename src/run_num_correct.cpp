#ifndef RUN_APPLICATION_CPP_
#define RUN_APPLICATION_CPP_

#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <glob.h>
#include <sys/mman.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <sys/stat.h>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "nnudictionary.hpp"
#include "tictoc.hpp"
#include "utilities.cpp"
#include "eigenio.hpp"
#include "batch_encode.cpp"

using namespace std;

//Calls the python script to train a dictionary
MatrixXd python_run_dictionary(cmd_arguments args, string tmp_tr_path) {
    ostringstream oss;
    MatrixXd D;
    oss << "python " << args.python_path << "train_dictionary.py"
        << " --num_clusters=" << args.num_atoms
        << " --method=" << args.cluster_method
        << " --data=" << tmp_tr_path
        << " --k=" << args.k
        << " --output=" << args.dictionary_path;

    string cmd_str = oss.str();
    system(cmd_str.c_str());

    EigenIO::read_npy_double((args.dictionary_path + ".npy").c_str(), D);

    return D;
}

//Trains a dictionary and caches it for further use
MatrixXd train_dictionary(cmd_arguments args, MatrixXd &D) {
    MatrixXd train_data;

    //Check for cached training data
    if(!file_exists(args.raw_data_path)) {
        train_data = extract_data(args.data_folder, args.train_file, args.num_tr_data,
                                  args.verbose);
        EigenIO::write_npy(args.raw_data_path.c_str(), train_data.transpose());
    } else {
        EigenIO::read_npy_double(args.raw_data_path.c_str(), train_data);
    }

    
    if (args.verbose)
        cout << "Learning Dictionary" << endl;

    //Train Dictionary
    string tmp_tr_path = args.output_path + "tr.npy";
    EigenIO::write_npy(tmp_tr_path.c_str(), train_data);
    D = python_run_dictionary(args, tmp_tr_path);

    return D;
}


void run_batch_encoder(cmd_arguments args, NNU::Dictionary* NNU_D, MatrixXd &vappt) {
    double pct_correct;
    vector<label_data> tr_data;
    vector<label_data> t_data;

    //Serial encoding
    if (args.verbose)
        cout << "Encoding training files (serial)\n";

    pct_correct = args.pcEncode(NNU_D, args.train_file, args, vappt);

    //Parallel encoding (only for the timing information)
    string ser_encoder = args.encoder;

    //output results
    cout << ser_encoder << ", "
         << args.num_multadd << ", "
         << args.num_atoms << ", "
         << args.alpha << ", "
         << args.beta << ", "
         << pct_correct
         << flush << endl;
}

//select the batch encoder to use (related to application)
void select_batch_encoder(cmd_arguments &args) {
    if (args.application == "kth") {
        args.pcEncode = bow_encode_pct_correct;
    } else {
        cout << "Not implemented" << endl;
        assert(false);
    }
}

void run_application(cmd_arguments args) {
    MatrixXd D;
    MatrixXd vappt;
    NNU::Dictionary* NNU_D;
    select_batch_encoder(args); //sets function pointer for batch encode

    //Check for cached dictionary
    if (!file_exists(args.dictionary_path + ".npy")) {
        D = train_dictionary(args, D);
    } else {
        if (args.verbose)
            cout << "Using cached Dictionary" << endl;

        EigenIO::read_npy_double((args.dictionary_path + ".npy").c_str(), D);
    }

    l2_norm(D);

    //Get V using X
    if (args.encoder == "nns-pca-x" || args.encoder == "mp-pca-x") {
        MatrixXd train_data = extract_data(args.data_folder, args.train_file,
                                           args.num_tr_data, false);
        int rows = min(int(train_data.rows()), args.max_pca_tr);

        //Create items for NNU Dictionary
        JacobiSVD<MatrixXd> svd(train_data.block(0, 0, rows, train_data.cols()), ComputeThinV);
        MatrixXd V = svd.matrixV();
        vappt = V.block(0, 0, V.rows(), args.alpha).transpose();

    } else { //Get V using D
        //Create items for NNU Dictionary
        JacobiSVD<MatrixXd> svd(D.transpose(), ComputeThinV);
        MatrixXd V = svd.matrixV();
        vappt = V.block(0, 0, V.rows(), args.alpha).transpose();
    }


    //Check for cached NNU dictionary
    if (!file_exists(args.NNU_path)) {
        if (args.verbose)
            cout << "Creating NNU Dictionary\n";

        NNU_D = NNU::train_dict(D, args.alpha, args.beta, args.verbose);
        NNU_D->save(args.NNU_path.c_str());
    } else {
        if (args.verbose)
                cout << "Loaded cached NNU Dictionary\n";

        NNU_D = new NNU::Dictionary(args.alpha, D.cols(), args.beta, D, vappt);
        NNU_D->load(args.NNU_path.c_str());
    }

    run_batch_encoder(args, NNU_D, vappt);
}

int main(int argc, char *argv[]) {
    /* Parse commandline args */
    cmd_arguments args(argc, argv);

    /* Turn off multi-threading for Eigen */
    Eigen::setNbThreads(1);

    /* Fix number of cores for parallelism */
    omp_set_num_threads(10);

    run_application(args);

    return 0;
}

#endif
