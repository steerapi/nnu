#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <glob.h>
#include <sys/mman.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <sys/stat.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "nnudictionary.hpp"
#include "nnumpcode.hpp"
#include "tictoc.hpp"
#include "eigenio.hpp"
#include "utilities.cpp"
#include "batch_encode.cpp"

using namespace std;

void run_test(cmd_arguments args) {
    double total_encode_time = 0.0;
    ostringstream oss;
    string cmd_str;
    MatrixXd D;
    NNU::Dictionary* NNU_D;

    if (file_exists(args.output_path + args.KM_path + ".npy")) {
        if (args.verbose)
            cout << "Loading learned centroids from cache\n";
    } else {
        MatrixXd train_data = extract_data(args.data_folder, args.train_file,
                                           args.KM_tr_data, args.verbose);
        string save_path = args.output_path + "KM_tr_data.npy";
        EigenIO::write_npy(save_path.c_str(), train_data);
        oss << "python " + args.python_path + "trainKM.py --num_clusters=" <<
               args.KM_clusters << " --data=" << save_path << " --output=" <<
               args.output_path + args.KM_path;

        cmd_str = oss.str();
        oss.str("");
        system(cmd_str.c_str());
    }

    string load_path = args.output_path + args.KM_path + ".npy";
    EigenIO::read_npy_double(load_path.c_str(), D);

    l2_norm(D);

    JacobiSVD<MatrixXd> svd(D.transpose(), ComputeFullV);
    MatrixXd V = svd.matrixV();

    MatrixXd vapp = V.block(0, 0, V.rows(), args.alpha);
    MatrixXd vappt = vapp.transpose();

    if (file_exists(args.output_path + args.NNU_path)) {
        if (args.verbose)
            cout << "Loaded cached NNU Dictionary\n";

        NNU_D = new NNU::Dictionary(args.alpha, D.cols(), args.beta, D, vappt);
        NNU_D->load((args.output_path + args.NNU_path).c_str());

    } else {
        if (args.verbose)
            cout << "Converting NNU Dictionary\n";

        NNU_D = NNU::train_dict(D, args.alpha, args.beta, args.verbose);
        NNU_D->save((args.output_path + args.NNU_path).c_str());
    }


    if (args.verbose)
        cout << "Encoding training files\n";

    vector<label_data> tr_data = pool_encode(NNU_D, args.train_file, args,
                                             total_encode_time);

    if (args.verbose)
        cout << "Encoding testing files\n";

    vector<label_data> t_data = pool_encode(NNU_D, args.test_file, args,
                                            total_encode_time);

    save_label_data(tr_data, args.labels_tr_path, args.data_tr_path);
    save_label_data(t_data, args.labels_t_path, args.data_t_path);

    oss << "python " + args.python_path + "predict.py --tr_data=" <<
        args.data_tr_path << " --tr_label=" << args.labels_tr_path <<
        " --t_data=" << args.data_t_path << " --t_label=" << args.labels_t_path;

    if (args.verbose)
        oss << " --verbose ";

    cout << args.encoder;

    cout << ", " << args.KM_clusters << ", ";

    cout << args.alpha << ", " << args.beta << ", " << std::flush;

    cmd_str = oss.str();
    system(cmd_str.c_str());

    if (args.verbose)
        cout << "Total encoding time: ";

    cout << total_encode_time << std::flush << '\n';
}


int main(int argc, char *argv[]) {
    /* Parse commandline args */
    cmd_arguments args(argc, argv);

    /* Turn off multi-threading for Eigen */
    Eigen::setNbThreads(1);

    run_test(args);

    return 0;
}
