#ifndef _PCA_H
#define _PCA_H

#include <iostream>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

class PCA {
private:
    int n_components;
    MatrixXd components;

public:
    PCA(int n_components) : n_components(n_components) {};

    void fit(MatrixXd X) {
        VectorXd row_means = X.rowwise().mean();
        for (int i = 0; i < X.rows(); ++i)
            X(i) -= row_means(i);

        JacobiSVD<MatrixXd> svd(X, ComputeThinV);
        MatrixXd V = svd.matrixV();
        components = V.block(0, 0, V.rows(), n_components);
    }

    MatrixXd transform(MatrixXd X) {
        /* cout << X.rows() << " " << X.cols() << endl; */
        /* cout << components.rows() << " " << components.cols() << endl; */
        VectorXd row_means = X.rowwise().mean();
        for (int i = 0; i < X.rows(); ++i)
            X(i) -= row_means(i);

        return X * components;
    }

    MatrixXd fit_transform(MatrixXd X) {
        fit(X);
        return transform(X);
    }

    MatrixXd get_components() {
        return components;
    }
};

#endif /*_PCA_H*/
