#ifndef NNUDICTIONARY_HPP_
#define NNUDICTIONARY_HPP_

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <set>
#include <mutex>
#include <thread>

#include "../hashlist/src/hashtable.hpp"
#include "half.hpp"
#include "sortindex.hpp"
#include <Eigen/Dense>
#include <boost/python.hpp>
#include <boost/numpy.hpp>
/* #include "utilities.cpp" */
#include "structs.h"

using namespace std;
using namespace Eigen;
using half_float::half;
using namespace boost::python;
namespace bn = boost::numpy;

namespace NNU {

class Dictionary {
private:
    uint16_t ***iram;
    int range;
    int top;
    MatrixXd D;
    MatrixXd vappt;
public:
    int cols;
    int rows;
    Dictionary() { };
    Dictionary(int rows, int cols, int top, MatrixXd D, MatrixXd vappt) :
            cols(cols), rows(rows), top(top), range(1 << 16),
            D(D), vappt(vappt) {
        iram = new uint16_t**[rows];
        for (int i = 0; i < rows; i++) {
            iram[i] = new uint16_t*[range];
            for (int j = 0; j < range; j++) {
                iram[i][j] = new uint16_t[top];
            }
        }
    }

    void set_params(int rows, int cols, int top) {
        this->rows = rows;
        this->cols = cols;
        this->top = top;
    }


    int get_rows() { return rows; }
    int get_cols() { return cols; }
    MatrixXd& get_D() { return D; }
    MatrixXd& get_vappt() { return vappt; }

    vector<size_t> atomLookupVoting(VectorXd X, int topAtoms) {
        vector<int> votes(cols);
        vector<size_t> idxs;
        for (int j = 0; j < rows; ++j) {
            double v = X(j);
            for (int k = 0; k < top; ++k) {
                votes[NNULookup(j, half(v).data_)[k]] += 1;
            }
        }
        vector<size_t> sIdxs = sort_indexes_max(votes);
        for (int k = 0; k < topAtoms; ++k) {
            idxs.push_back(sIdxs[k]);
        }
        return idxs;
    }

    vector<double> ts;
    vector<size_t> adaptiveAtomLookup(VectorXd &X, int* weights) {
        set<size_t> idxs;
        for (int j = 0; j < rows; ++j) {
            double v = X(j);
            for (int k = 0; k < weights[j]; ++k) {
                idxs.insert(NNULookup(j, half(v).data_)[k]);
            }
        }
        return vector<size_t>(idxs.begin(), idxs.end());
    }

    vector<size_t> atomLookup(VectorXd &X) {
        set<size_t> idxs;
        for (int j = 0; j < rows; ++j) {
            double v = X(j);
            uint16_t *iram = NNULookup(j, half(v).data_);
            for (int k = 0; k < top; ++k) {
                idxs.insert(iram[k]);
            }
        }
        return vector<size_t>(idxs.begin(), idxs.end());
    }

    VectorXd batchParTableAtomLookup(MatrixXd &X, MatrixXd &D, MatrixXd &vapptX,
            int nthreads_alpha = 1, int nthreads_beta = 1) {
        // WARN: rows should be divisible by nthreads
        Eigen::initParallel();
        // Create N threads
        vector<thread> threads;
        int perthread = rows / nthreads_alpha;
        int numX = X.cols();
        VectorXd ret = VectorXd::Zero(numX);
        VectorXd retmax = VectorXd::Zero(numX);

        for (int i = 0; i < nthreads_alpha; ++i) {
            threads.push_back(thread([&,i]{
                int skip = i * perthread;
                for (int l = 0; l < numX; ++l) {
                    VectorXd Xl = X.col(l);
                    VectorXd vapptXl = vapptX.col(l);
                    for (int j = 0; j < perthread; ++j) {
                        int rowIdx = skip + j;
                        double v = vapptXl(rowIdx);
                        uint16_t *iram = NNULookup(rowIdx, half(v).data_);
                    for (int k = 0; k < top; ++k) {
                        int idx = iram[k];
                        double tmpcoeff = std::abs(Xl.dot(D.col(idx)));
                        if (tmpcoeff > retmax(l)) {
                            retmax(l) = tmpcoeff;
                            ret(l) = idx;
                        }
                    }
                }
            }
        }));
        }

        // Synchronize threads:
        for (auto &thread : threads) {
            thread.join();
        }
        return ret;
    }

    bn::ndarray batchParTableAtomLookupNd(Map<MatrixXd> &X, MatrixXd &D,
                                          MatrixXd &vapptX,
                                          int nthreads_alpha = 1,
                                          int nthreads_beta = 1) {
        // WARN: rows should be divisible by nthreads
        Eigen::initParallel();
        // Create N threads
        vector<thread> threads;
        int perthread = rows / nthreads_alpha;
        int numX = X.cols();
        Py_intptr_t shape[1] = { numX };
        VectorXd retv = VectorXd::Zero(numX);
        VectorXd retmax = VectorXd::Zero(numX);
        bn::ndarray ret = bn::zeros(1, shape, bn::dtype::get_builtin<double>());

        for (int i = 0; i < nthreads_alpha; ++i) {
            threads.push_back(thread([&,i] {
                int skip = i * perthread;
                for (int l = 0; l < numX; ++l) {
                    VectorXd Xl = X.col(l);
                    VectorXd vapptXl = vapptX.col(l);
                    for (int j = 0; j < perthread; ++j) {
                        int rowIdx = skip + j;
                        double v = vapptXl(rowIdx);
                        uint16_t *iram = NNULookup(rowIdx, half(v).data_);
                    for (int k = 0; k < top; ++k) {
                        int idx = iram[k];
                        double tmpcoeff = std::abs(Xl.dot(D.col(idx)));
                        if (tmpcoeff > retmax(l)) {
                            retmax(l) = tmpcoeff;
                            retv[l] = idx;
                        }
                    }
                }
            }
        }));
        }

        // Synchronize threads:
        for (auto &thread : threads) {
            thread.join();
        }

        for (int i = 0; i < numX; ++i) {
            ret[i] = retv(i);
        }

        return ret;
    }

    bn::ndarray batchParTableAtomLookupNd(MatrixXd &X, MatrixXd &D,
                                          MatrixXd &vapptX,
                                          int nthreads_alpha = 1,
                                          int nthreads_beta = 1) {
        // WARN: rows should be divisible by nthreads
        Eigen::initParallel();
        // Create N threads
        vector<thread> threads;
        int perthread = rows / nthreads_alpha;
        int numX = X.cols();
        Py_intptr_t shape[1] = { numX };
        VectorXd retv = VectorXd::Zero(numX);
        VectorXd retmax = VectorXd::Zero(numX);
        bn::ndarray ret = bn::zeros(1, shape, bn::dtype::get_builtin<double>());

        for (int i = 0; i < nthreads_alpha; ++i) {
            threads.push_back(thread([&,i] {
                int skip = i * perthread;
                for (int l = 0; l < numX; ++l) {
                    VectorXd Xl = X.col(l);
                    VectorXd vapptXl = vapptX.col(l);
                    for (int j = 0; j < perthread; ++j) {
                        int rowIdx = skip + j;
                        double v = vapptXl(rowIdx);
                        uint16_t *iram = NNULookup(rowIdx, half(v).data_);
                    for (int k = 0; k < top; ++k) {
                        int idx = iram[k];
                        double tmpcoeff = std::abs(Xl.dot(D.col(idx)));
                        if (tmpcoeff > retmax(l)) {
                            retmax(l) = tmpcoeff;
                            retv[l] = idx;
                        }
                    }
                }
            }
        }));
        }

        // Synchronize threads:
        for (auto &thread : threads) {
            thread.join();
        }

        for (int i = 0; i < numX; ++i) {
            ret[i] = retv(i);
        }

        return ret;
    }


    vector<size_t> parTableAtomLookup(VectorXd &X) {
        //  Create N threads
        vector<thread> threads;
        HashTable<size_t> hashtable(cols);
        int nthreads = rows; // WARN: rows should be divisible by nthreads
        int perthread = rows / nthreads;
        for (int i = 0; i < nthreads; ++i) {
            int skip = i * perthread;
            threads.push_back(thread([&] {
                for (int j = 0; j < perthread; ++j) {
                    double v = X(skip + j);
                    uint16_t *iram = NNULookup(skip + j, half(v).data_);
                    for (int k = 0; k < top; ++k) {
                        hashtable.insert(iram[k]);
                    }
                }
            }));
        }
        // Synchronize threads:
        for (auto &thread : threads) {
            thread.join();
        }

        return hashtable.get();
    }

    void load(const char* filename) {
        ifstream file;
        file.open(filename, ios::in | ios::binary);
        if (file.is_open()) {
            for (int j = 0; j < rows; ++j) {
                for (int i = 0; i < range; ++i) {
                    for (int k = 0; k < top; ++k) {
                        file.read(reinterpret_cast<char*>(&iram[j][i][k]),
                                sizeof(uint16_t));

                    }
                }
            }
            file.close();
        }
    }
    void save(const char* filename) {
        ofstream file;
        file.open(filename, ios::out | ios::binary);
        if (file.is_open()) {
            for (int j = 0; j < rows; ++j) {
                for (int i = 0; i < range; ++i) {
                    for (int k = 0; k < top; ++k) {
                        file.write(reinterpret_cast<char*>(&iram[j][i][k]),
                                sizeof(uint16_t));
                    }
                }
            }
            file.close();
        }
    }

    /**
     * Convert from double to half and put in the iram
     */
    void buildNNUDictionaryFromMatrix(MatrixXd &vtd, bool verbose) {
        double pct_done = 0.1;
        double start_t = get_wall_time();
        int completed = 0;

        omp_lock_t print_lock;
        omp_init_lock(&print_lock);

        #pragma omp parallel for
        for (uint32_t iramvalue = 0; iramvalue < range; ++iramvalue) {
            half v;
            v.data_ = iramvalue;
            double iramdouble = v;
            for (int j = 0; j < rows; ++j) {
                std::vector<double> c(cols, 0.0);
                for (int i = 0; i < cols; i++) {
                    double v = vtd(j, i);
                    c[i] = std::abs(v - iramdouble);
                }
                vector<size_t> idxs = sort_indexes(c);
                for (int k = 0; k < top; ++k) {
                    iram[j][iramvalue][k] = idxs[k];
                }
            }

            omp_set_lock(&print_lock);
            completed++;
            if (verbose && (double) completed / range > pct_done) {
                cout << '\r' << pct_done * 100 << "\%, Elapsed: " <<
                        get_wall_time() - start_t << flush;
                pct_done += 0.1;
            }
            omp_unset_lock(&print_lock);
        }
        omp_destroy_lock(&print_lock);
        cout << endl;
    }

    uint16_t *NNULookup(int row, uint16_t x) {
        return iram[row][x];
    }



    ~Dictionary() {
        for (int j = 0; j < rows; j++) {
            for (int i = 0; i < range; i++) {
                delete iram[j][i];
            }
            delete iram[j];
        }
        delete[] iram;
    }
};


Dictionary* train_dict(MatrixXd D, int alpha, int beta, bool verbose) {
    JacobiSVD<MatrixXd> svd(D.transpose(), ComputeFullV);
    MatrixXd V = svd.matrixV();

    int dSize = D.cols();
    MatrixXd vapp = V.block(0, 0, V.rows(), alpha);
    MatrixXd vappt = vapp.transpose();
    MatrixXd vapptd = vappt * D;

    Dictionary* vtdiram = new Dictionary(alpha, dSize, beta, D, vappt);
    vtdiram->buildNNUDictionaryFromMatrix(vapptd, verbose);

    return vtdiram;
}

}

#endif /* DICTIONARY_HPP_ */
