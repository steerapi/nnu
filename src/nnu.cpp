//============================================================================
// Name        : nnu.cpp
// Author      : Surat Teerapittayanon
// Version     : 1.5
//============================================================================

#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sys/mman.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "nnudictionary.hpp"
#include "nnumpcode.hpp"
#include "tictoc.hpp"
#include "eigenio.hpp"

using namespace std;

#define MULTI_LINE_STRING(a) #a

typedef SparseVector<double> SparseXd;
typedef SparseMatrix<double> SparseMatrixXd;

void rundict(char* dictpath, int comp, int neig, char* outputpath) {

	MatrixXd D;
	EigenIO::read_binary(dictpath, D);

	JacobiSVD<MatrixXd> svd(D.transpose(), ComputeFullV);
	MatrixXd V = svd.matrixV();

	int dSize = D.cols();
	MatrixXd vapp = V.block(0, 0, V.rows(), comp);
	MatrixXd vapptd = vapp.transpose() * D;

	//Build NNU
	NNU::Dictionary *vtdiram = new NNU::Dictionary(comp, dSize, neig, true);
	vtdiram->buildNNUDictionaryFromMatrix(vapptd);
	string dictpathstr = dictpath;

	stringstream sstm;
	if (outputpath == NULL) {
		sstm << dictpath << "_c" << comp << "_n" << neig << ".nnu";
	} else {
		sstm << outputpath << "/dict.nnu";
	}
	vtdiram->save((char*) sstm.str().c_str());
}

void runcode(char* dictpath, char* datapath, char* nnupath, int comp, int neig,
		int k, char* outputpath) {
	MatrixXd D, X;
	EigenIO::read_binary(dictpath, D);
	EigenIO::read_binary(datapath, X);

	JacobiSVD<MatrixXd> svd(D.transpose(), ComputeFullV);
	MatrixXd V = svd.matrixV();

	int dSize = D.cols();
	MatrixXd vapp = V.block(0, 0, V.rows(), comp);
	MatrixXd vappt = vapp.transpose();

	NNU::Dictionary *vtdiram = new NNU::Dictionary(comp, D.cols(), neig, true);
	vtdiram->load(nnupath);

	// Load Data
	MatrixXd zNNU(dSize, X.cols());
	int cols = X.cols();
	mlockall(MCL_CURRENT | MCL_FUTURE);
	//#pragma omp parallel num_threads(8)
	//#pragma omp for
	register int i;
	for (i = 0; i < cols; ++i) {
		VectorXd cX = X.col(i);
		SparseXd S = NNU::mpcode(k, vtdiram, vappt, cX, D);
		zNNU.col(i) = VectorXd(S);
//		cout << (double) i / cols << endl;
	}
	munlockall();

	//Compute error
	double meanmse = NNU::computeMSE(cols, X, D, zNNU);
	cout << "MSE: " << meanmse << endl;
	stringstream sstm;
	if (outputpath == NULL) {
		sstm << datapath << "_c" << comp << "_n" << neig << "-code.bin";
	} else {
		sstm << outputpath << "/code.bin";
	}
	EigenIO::write_binary(sstm.str().c_str(), zNNU);
}

void printhelp(){
	cout << "\nnnu [options]\n"
			"\n"
			"  -m [dict|code]\n"
			"  mode (required)\n"
			"  -d path\n"
			"  path (required)\n"
			"  -c\n"
			"  the number of PCA components (< #rows of the dictionary)\n"
			"  -n\n"
			"  the number of voting neighbors (< #cols of the dictionary)\n"
			"  -o path\n"
			"  output folder (optional)\n"
			"  -x path\n"
			"  input data path (required in 'code' mode)\n"
			"  -u path\n"
			"  nnu path (required in 'code' mode)\n"
			"  -k\n"
			"  the number of iterations for coding\n"
			"  -h\n"
			"  print help text\n\n";
}

int main(int argc, char *argv[]) {
	int option = -1;
	char* mode;
	char* dictpath;
	char* outputpath;
	char* datapath;
	char* nnupath;
	int comp = 1;
	int neig = 1;
	int k = 1;

	while ((option = getopt(argc, argv, "m:d:c:n:o:x:u:k:h")) != -1) {
		switch (option) {
		case 'm':
			// mode [dict|code] (required)
			mode = strdup(optarg);
			break;
		case 'd':
			// Eigen dictionary path. Binary double precision. Row col data. (required)
			dictpath = strdup(optarg);
			break;
		case 'c':
			// The number of PCA components
			comp = atoi(optarg);
			break;
		case 'n':
			// The number of voting neighbors
			neig = atoi(optarg);
			break;
		case 'o':
			// Output path
			outputpath = strdup(optarg);
			break;
		case 'x':
			// Eigen data input path Binary double precision. Row col data. (required for code)
			datapath = strdup(optarg);
			break;
		case 'u':
			// nnu path (required for code)
			nnupath = strdup(optarg);
			break;
		case 'k':
			// The number of iterations for sparse coding
			k = atoi(optarg);
			break;
		default:
			// unrecognised option
			printhelp();
			return 0;
			break;
		}
	}
	if (mode == NULL) {
		cout << "Invalid mode" << endl;
		printhelp();
		return -1;
	}
	if (dictpath == NULL) {
		cout << "Invalid dictionary input path" << endl;
		printhelp();
		return -1;
	}
	// rest of the program
	tic();
	if (strcmp(mode, "dict") == 0) {
		rundict(dictpath, comp, neig, outputpath);
	} else if (strcmp(mode, "code") == 0) {
		runcode(dictpath, datapath, nnupath, comp, neig, k, outputpath);
	} else {
		printhelp();
		return 0;
	}
	toc();
	return 0;
}
