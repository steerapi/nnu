/* Command line wrapper around nnu */
#ifndef NNU_CMDL_CPP_
#define NNU_CMDL_CPP_

#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <glob.h>
#include <sys/mman.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <sys/stat.h>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "nnudictionary.hpp"
#include "tictoc.hpp"
#include "utilities.cpp"
#include "eigenio.hpp"
#include "batch_encode.cpp"

using namespace std;

void help() {
    cout << "write..\n";
}

void encode_nnu(string encoder, string tr_path, string dictionary_path,
                string NNU_path, string output_path, int alpha, int beta) {
    MatrixXd D = EigenIO::readCSV(dictionary_path);
    MatrixXd x = EigenIO::readCSV(tr_path);
    VectorXd z;


    JacobiSVD<MatrixXd> svd(D.transpose(), ComputeThinV);
    MatrixXd V = svd.matrixV();
    MatrixXd vappt = V.block(0, 0, V.rows(), alpha).transpose();
    static MatrixXd vapptD = vappt * D;



    NNU::Dictionary* NNU_D = new NNU::Dictionary(alpha, D.cols(), beta, D, vappt);
    NNU_D->load(NNU_path.c_str());


    double start_t = get_wall_time();

    if (encoder == "nns") {
            z = NNU::nns(NNU_D, vappt, x, D);
        } else if (encoder == "par-nns") {
            z = NNU::par_nns(NNU_D, vappt, x, D);
        } else if (encoder == "nns-pca-x") {
            z = NNU::nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (encoder == "par-nns-pca-x") {
            z = NNU::par_nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (encoder == "nns-pca-d") {
            z = NNU::nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (encoder == "par-nns-pca-d") {
            z = NNU::par_nns_pca(NNU_D, vappt, vapptD, x, D);
        } else if (encoder == "nnu") {
            z = NNU::nnu(NNU_D, vappt, x, D);
        } else if (encoder == "par-nnu") {
            z = NNU::par_nnu(NNU_D, vappt, x, D);
        } else if (encoder == "nnu-pca") {
            z = NNU::nnu_pca(NNU_D, vappt, vapptD, x, D);
        } else if (encoder == "par-nnu-pca") {
            z = NNU::par_nnu_pca(NNU_D, vappt, vapptD, x, D);
        } else {
            cout << "Invalid encoder for NN.\n";
            exit(1);
        }

    //print timing info
    cout << get_wall_time() - start_t << endl;

    EigenIO::writeToCSVfile(output_path, z);
}

void encode_mp(string encoder, string tr_path, string dictionary_path,
               string NNU_path, string output_path, int alpha, int beta, int k) {
    MatrixXd D = EigenIO::readCSV(dictionary_path);
    MatrixXd x = EigenIO::readCSV(tr_path);
    MatrixXd z;


    JacobiSVD<MatrixXd> svd(D.transpose(), ComputeThinV);
    MatrixXd V = svd.matrixV();
    MatrixXd vappt = V.block(0, 0, V.rows(), alpha).transpose();
    static MatrixXd vapptD = vappt * D;

    NNU::Dictionary* NNU_D = new NNU::Dictionary(alpha, D.cols(), beta, D, vappt);
    NNU_D->load(NNU_path.c_str());

    double start_t = get_wall_time();

    //perform encoding
    if (encoder == "mp") {
        z = NNU::mp(NNU_D, vappt, x, D, k);
    } else if (encoder == "par-mp") {
        z = NNU::par_mp(NNU_D, vappt, x, D, k);
    } else if (encoder == "mp-pca-x") {
        z = NNU::mp_pca(NNU_D, vappt, x, D, k);
    } else if (encoder == "par-mp-pca-x") {
        z = NNU::par_mp_pca(NNU_D, vappt, x, D, k);
    } else if (encoder == "mp-pca-d") {
        z = NNU::mp_pca(NNU_D, vappt, x, D, k);
    } else if (encoder == "par-mp-pca-d") {
        z = NNU::par_mp_pca(NNU_D, vappt, x, D, k);
    } else if (encoder == "nnump") {
        z = NNU::nnump(NNU_D, vappt, x, D, k);
    } else if (encoder == "par-nnump") {
        z = NNU::par_nnump(NNU_D, vappt, x, D, k);
    } else if (encoder == "nnump-pca") {
        z = NNU::nnump_pca(NNU_D, vappt, x, D, k);
    } else if (encoder == "par-nnump-pca") {
        z = NNU::par_nnump_pca(NNU_D, vappt, x, D, k);
    } else {
        cout << "Invalid encoder for MP.\n";
        exit(1);
    }

    //print timing info
    cout << get_wall_time() - start_t << endl;

    EigenIO::writeToCSVfile(output_path, z);
}


void train(string dictionary_path, string output_path, int alpha, int beta) {
    MatrixXd D = EigenIO::readCSV(dictionary_path);
    double start_t = get_wall_time();
    NNU::Dictionary* NNU_D = NNU::train_dict(D, alpha, beta, false);
    cout << get_wall_time() - start_t << endl;
    NNU_D->save(output_path.c_str());
}


int main(int argc, char *argv[]) {
    int alpha, beta, k, num_atoms;
    string method, tr_path, output_path, encoder, dictionary_path, NNU_path;

    int option = -1;
    string arg_str = "hm:n:d:e:o:r:a:b:k:v";

    while ((option = getopt(argc, argv, arg_str.c_str())) != -1) {
        switch (option) {
        case 'm':
            method = string(strdup(optarg));
            break;
        case 'n':
            NNU_path = string(strdup(optarg));
            break;
        case 'd':
            dictionary_path = string(strdup(optarg));
            break;
        case 'e':
            encoder = string(strdup(optarg));
            break;
        case 'o':
            output_path = string(strdup(optarg));
            break;
        case 'r':
            tr_path = string(strdup(optarg));
            break;
        case 'a':
            alpha = atoi(optarg);
            break;
        case 'b':
            beta = atoi(optarg);
            break;
        case 'k':
            k = atoi(optarg);
            break;
        case 'h':
            help();
            exit(0);
            break;
        default:
            help();
            exit(1);
            break;
        }
    }

    /* Turn off multi-threading for Eigen */
    Eigen::setNbThreads(1);

    /* Fix number of cores for parallelism */
    omp_set_num_threads(10);

    if (method == "train") {
        train(dictionary_path, output_path, alpha, beta);
    } else if (method == "nn") {
        encode_nnu(encoder, tr_path, dictionary_path, NNU_path, output_path,
                   alpha, beta);
    } else if (method == "mp") {
        encode_mp(encoder, tr_path, dictionary_path, NNU_path, output_path,
                  alpha, beta, k);
    }

    return 0;
}

#endif
