# Parallel Nearest Neighbor Unit (PNNU)
Approximate Nearest Neighbor Search algorithms. 
Includes both a C++ and python interface.

## Dependencies
    C++11
    Eigen3
    Boost
    Boost.NumPy

## Building C++ source
To build the C++ examples, do the following from the root directory:

    mkdir build
    cd build
    cmake ..

This will build the executable run_application, that 
runs the three applications using each encoders discussed in the paper. 
If you are only interested in PNNU functions, they are completely contained in 
nnudictionary.hpp and nnumpcode.hpp. You can run the executable in the 
following manner (-h flag for full option list):

### Sample run
    ./bin/run_application -p lena -e mp-pca-d -s linear -d data/lena/ -o output/ -r data/lena.txt -c KSVD -y python/ -w 1000 -q 100000 -a 1 -b 1 -k 5 -v
 


## Building python Interface
To build the python interface, do the following from the root directory:

    cd shared/
    mkdir build
    cd build
    cmake ..

This will create a nnu.so shared object file in the build directory. After adding it to your python path (or making sure it is in the same directory as the python file you wish to run), you can import nnu as you would any python module. You can use the library as follows:

    import nnu
    import numpy as np
    from time import time

    D = np.random.random((100, 40))
    data = np.random.random((5000, 40))
    datat = data.T
    alpha = 5
    beta = 5

    #constructs the NNU lookup tables
    nnu_d = nnu.build_dict(D, alpha, beta, True)

    #exhaustive nearest neighbor search
    start = time()
    enc_data = nnu.nns(nnu_d, data)
    print 'exhaustive nns:', time() - start

    #nnu search
    start = time()
    enc_data = nnu.nnu(nnu_d, data)
    print 'nnu:', time() - start


For a full example including all NNU methods look at shared/example.py