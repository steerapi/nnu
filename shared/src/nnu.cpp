/*
 * nnu-python.cpp
 *
 *  Created on: Apr 22, 2015
 *      Authors: surat, brad
 */

#include <sstream>
#include <algorithm>
#include <thread>
#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "../../src/nnudictionary.hpp"
#include <boost/python.hpp>
#include <boost/numpy.hpp>

using namespace Eigen;
using namespace std;
using namespace boost::python;
namespace bn = boost::numpy;

vector<size_t> candidatesLookup(VectorXd &vtx, NNU::Dictionary *vtd) {

    /* return vtd->parTableAtomLookup(vtx); */
    return vtd->atomLookup(vtx);

}

int computeMaxDot(VectorXd &cX, MatrixXd &D) {
    double maxcoeff = 0.0;
    double tmpcoeff = 0.0;
    int maxidx = 0.0;
    int cols = D.cols();
    for (int idx = 0; idx < cols; ++idx) {
        tmpcoeff = std::abs(cX.dot(D.col(idx)));
        if (tmpcoeff > maxcoeff) {
            maxcoeff = tmpcoeff;
            maxidx = idx;
        }
    }
    return maxidx;
}

int computeCtX(vector<size_t> &atomIdxs, VectorXd &cX, MatrixXd &D) {
    double maxcoeff = 0.0;
    double tmpcoeff = 0.0;
    int maxidx = 0.0;
    for (auto idx : atomIdxs) {
        tmpcoeff = std::abs(cX.dot(D.col(idx)));
        if (tmpcoeff > maxcoeff) {
            maxcoeff = tmpcoeff;
            maxidx = idx;
        }
    }

    return maxidx;
}

MatrixXd fillMatrix(bn::ndarray& npdata) {
    double* data = reinterpret_cast<double*>(npdata.get_data());
    int rows = npdata.shape(0);
    int cols = npdata.shape(1);
    MatrixXd ret(rows, cols);

    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < cols; ++j)
            ret(i, j) = data[i * cols + j];

    return ret;
}

MatrixXd fillMatrixT(bn::ndarray& npdata) {
    double* data = reinterpret_cast<double*>(npdata.get_data());
    int cols = npdata.shape(0);
    int rows = npdata.shape(1);
    MatrixXd ret(rows, cols);

    for (int i = 0; i < cols; ++i)
        for (int j = 0; j < rows; ++j)
            ret(j, i) = data[i * rows + j];

    return ret;
}


Map<MatrixXd> refMatrix(bn::ndarray& npdata) {
    double* data = reinterpret_cast<double*>(npdata.get_data());
    int rows = npdata.shape(0);
    int cols = npdata.shape(1);
    Map<MatrixXd> ret(data, rows, cols);

    return ret;
}

Map<MatrixXd> refMatrixT(bn::ndarray& npdata) {
    double* data = reinterpret_cast<double*>(npdata.get_data());
    int cols = npdata.shape(0);
    int rows = npdata.shape(1);
    Map<MatrixXd> ret(data, rows, cols);

    return ret;
}


NNU::Dictionary* build_dict(bn::ndarray& D_np, int alpha, int beta,
                            bool verbose) {
    MatrixXd D = fillMatrixT(D_np);
    JacobiSVD<MatrixXd> svd(D.transpose(), ComputeFullV);
    MatrixXd V = svd.matrixV();

    int dSize = D.cols();
    MatrixXd vapp = V.block(0, 0, V.rows(), alpha);
    MatrixXd vappt = vapp.transpose();
    MatrixXd vapptd = vappt * D;

    //Build NNU
    NNU::Dictionary* vtdiram = new NNU::Dictionary(alpha, dSize, beta, D, vappt);
    vtdiram->buildNNUDictionaryFromMatrix(vapptd, verbose);

    return vtdiram;
}

/* Exhaustive Nearest Neighbor Search */
bn::ndarray nns(NNU::Dictionary* vtd, bn::ndarray& X_np) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    VectorXd cX;
    int cols = X.cols();

    Py_intptr_t shape[1] = { cols };
    bn::ndarray ret = bn::empty(1, shape, bn::dtype::get_builtin<double>());

    for (int i = 0; i < cols; ++i) {
        cX = X.col(i);
        ret[i] = computeMaxDot(cX, D);
    }

    return ret;
}

bn::ndarray nnu(NNU::Dictionary* vtd, bn::ndarray& X_np) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    MatrixXd& vappt = vtd->get_vappt();

    int maxidx = 0;
    int cols = X.cols();
    double maxcoeff = 0.0;
    double tmpcoeff;
    vector<size_t> atomIdxs;
    VectorXd vtx;

    Py_intptr_t shape[1] = { cols };
    bn::ndarray ret = bn::empty(1, shape, bn::dtype::get_builtin<double>());

    for (int i = 0; i < cols; ++i) {
        vtx = vappt * X.col(i);
        atomIdxs = candidatesLookup(vtx, vtd);
        for (auto idx : atomIdxs) {
            tmpcoeff = std::abs(X.col(i).dot(D.col(idx)));
            if (tmpcoeff > maxcoeff) {
                maxcoeff = tmpcoeff;
                maxidx = idx;
            }
        }
        ret[i] = maxidx;
        maxcoeff = 0.0;
    }

    return ret;
}

bn::ndarray nnu_par(NNU::Dictionary* vtd, bn::ndarray& X_np) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    MatrixXd& vappt = vtd->get_vappt();

    MatrixXd vapptX = vappt * X;
    unsigned concurentThreadsSupported = thread::hardware_concurrency();
    unsigned rows = vtd->rows;
    return vtd->batchParTableAtomLookupNd(X, D, vapptX,
            std::min(concurentThreadsSupported, rows), vtd->cols);
}

bn::ndarray nnu_pca(NNU::Dictionary* vtd, bn::ndarray& X_np) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    MatrixXd& vappt = vtd->get_vappt();

    int maxidx = 0;
    int cols = X.cols();
    int drows = vtd->rows;
    int dcols = vtd->cols;
    double maxcoeff = 0.0;
    double tmpcoeff = 0.0;
    vector<size_t> atomIdxs;
    VectorXd vtx;
    static MatrixXd vapptD = vappt * D;

    Py_intptr_t shape[1] = { cols };
    bn::ndarray ret = bn::empty(1, shape, bn::dtype::get_builtin<double>());

    for (int i = 0; i < cols; ++i) {
        vtx = vappt * X.col(i);
        atomIdxs = candidatesLookup(vtx, vtd);
        for (auto idx : atomIdxs) {
            tmpcoeff = std::abs(vtx.dot(vapptD.col(idx)));

            if (tmpcoeff >= maxcoeff) {
                maxcoeff = tmpcoeff;
                maxidx = idx;
            }
        }
        ret[i] = maxidx;
        maxcoeff = 0.0;
    }

    return ret;
}


bn::ndarray nnu_par_pca(NNU::Dictionary* vtd, bn::ndarray& X_np) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    MatrixXd& vappt = vtd->get_vappt();

    MatrixXd vapptX = vappt * X;
    MatrixXd vapptD = vappt * D;
    unsigned concurentThreadsSupported = thread::hardware_concurrency();
    unsigned rows = vtd->rows;
    return vtd->batchParTableAtomLookupNd(vapptX, vapptD, vapptX,
            std::min(concurentThreadsSupported, rows), vtd->cols);
}



bn::ndarray mp(NNU::Dictionary* vtd, bn::ndarray& X_np, int k) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    MatrixXd& vappt = vtd->get_vappt();

    int max;
    int cols = X.cols();
    int actualIdx;
    double dotCoeff;
    VectorXd cX;
    VectorXd atom;

    Py_intptr_t shape[2] = { cols, D.cols() };
    bn::ndarray z = bn::zeros(2, shape, bn::dtype::get_builtin<double>());

    for (int i = 0; i < cols; ++i) {
        cX = X.col(i);

        for (int j = 0; j < k; ++j) {
            actualIdx = computeMaxDot(cX, D);

            //Compute residual
            atom = D.col(actualIdx);
            dotCoeff = cX.dot(atom);
            z[i][actualIdx] += dotCoeff;
            cX -= dotCoeff * atom;
        }

    }

    return z;
}


bn::ndarray mp_nnu(NNU::Dictionary* vtd, bn::ndarray& X_np, int k) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    MatrixXd& vappt = vtd->get_vappt();

    int max;
    int cols = X.cols();
    int actualIdx;
    double dotCoeff;
    vector<size_t> atomIdxs;
    VectorXd vtx;
    VectorXd cX;
    VectorXd atom;

    Py_intptr_t shape[2] = { cols, D.cols() };
    bn::ndarray z = bn::zeros(2, shape, bn::dtype::get_builtin<double>());

    for (int i = 0; i < cols; ++i) {
        cX = X.col(i);

        for (int j = 0; j < k; ++j) {
            vtx = vappt * cX;
            atomIdxs = candidatesLookup(vtx, vtd);
            actualIdx = computeCtX(atomIdxs, cX, D);

            //Compute residual
            atom = D.col(actualIdx);
            dotCoeff = cX.dot(atom);
            z[i][actualIdx] += dotCoeff;
            cX -= dotCoeff * atom;
        }
    }

    return z;
}

bn::ndarray mp_nnu_pca(NNU::Dictionary* vtd, bn::ndarray& X_np, int k) {
    Map<MatrixXd> X = refMatrixT(X_np);
    MatrixXd& D = vtd->get_D();
    MatrixXd& vappt = vtd->get_vappt();

    int max;
    int cols = X.cols();
    int actualIdx;
    double dotCoeff;
    vector<size_t> atomIdxs;
    VectorXd vtx;
    VectorXd cX;
    VectorXd atom;
    static MatrixXd vapptD = vappt * D;


    Py_intptr_t shape[2] = { cols, D.cols() };
    bn::ndarray z = bn::zeros(2, shape, bn::dtype::get_builtin<double>());

    for (int i = 0; i < cols; ++i) {
        cX = X.col(i);

        for (int j = 0; j < k; ++j) {
            vtx = vappt * cX;
            atomIdxs = candidatesLookup(vtx, vtd);
            actualIdx = computeCtX(atomIdxs, vtx, vapptD);

            //Compute residual
            atom = D.col(actualIdx);
            dotCoeff = cX.dot(atom);
            z[i][actualIdx] += dotCoeff;
            cX -= dotCoeff * atom;
        }
    }

    return z;
}


BOOST_PYTHON_MODULE(nnu) {
    bn::initialize();
    def("build_dict", &build_dict, return_value_policy<manage_new_object>());
    def("nns", &nns, return_value_policy<return_by_value>());
    def("nnu", &nnu, return_value_policy<return_by_value>());
    def("nnu_pca", &nnu_pca, return_value_policy<return_by_value>());
    def("nnu_par", &nnu_par, return_value_policy<return_by_value>());
    def("nnu_par_pca", &nnu_par_pca, return_value_policy<return_by_value>());
    def("mp", &mp, return_value_policy<return_by_value>());
    def("mp_nnu", &mp_nnu, return_value_policy<return_by_value>());
    def("mp_nnu_pca", &mp_nnu_pca, return_value_policy<return_by_value>());


    class_<NNU::Dictionary>("NNUDictionary")
        .add_property("cols", &NNU::Dictionary::get_cols)
        .add_property("rows", &NNU::Dictionary::get_rows);
}
