import nnu
import numpy as np
from time import time

D = np.random.random((100, 40))
data = np.random.random((5000, 40))
D = D / np.linalg.norm(D, axis=1)[:, np.newaxis]
data = data / np.linalg.norm(data, axis=1)[:, np.newaxis]
datat = data.T
alpha = 5
beta = 5

#constructs the NNU lookup tables
nnu_d = nnu.build_dict(D, alpha, beta, True)

#exhaustive nearest neighbor search
start = time()
enc_data = nnu.nns(nnu_d, data)
print 'exhaustive nns:', time() - start

#nnu search
start = time()
enc_data = nnu.nnu(nnu_d, data)
print 'nnu:', time() - start

start = time()
enc_data = nnu.nnu_pca(nnu_d, data)
print 'nnu_pca:', time() - start

start = time()
enc_data = nnu.nnu_par(nnu_d, data)
print 'nnu_par:', time() - start

start = time()
enc_data = nnu.nnu_par_pca(nnu_d, data)
print 'nnu_par_pca:', time() - start

start = time()
enc_data = nnu.mp(nnu_d, data, 5)
print 'mp:', time() - start

start = time()
enc_data = nnu.mp_nnu(nnu_d, data, 5)
print 'mp_nnu:', time() - start

start = time()
enc_data = nnu.mp_nnu_pca(nnu_d, data, 5)
print 'mp_nnu_pca:', time() - start
